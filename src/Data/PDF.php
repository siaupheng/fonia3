<?php

/*******************************************************************

    Module        : /Data/PDF.php
    Desc.         : v3 - Class pembuatan file PDF
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : April 18th, 2015.
    Last Modified : May 8th, 2021.

    (c) 2015 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Data;

use Mpdf;

final class PDF {
    private $__avalue = array();
    private $__atabel = array();
    private $__theader = "";
    private $__tcolname = "";
    
    public function __construct() {
    }

    public function add_field($tnama="", $nlebar=0, $talign="L", $ffunc="", $nkol=1) {
        $this->__atabel[] = array($tnama, $nlebar.'%', $ffunc, $talign, $nkol);
    }

    public function show_fields() {
        $tmp_out = "<thead><tr>";
        for ($i=0; $i<count($this->__atabel); $i++) {
            if ($this->__atabel[$i][4]==0) continue;
            $tmp_out .= $this->set_align($this->__atabel[$i][0], $this->__atabel[$i][1], "C", false, true, $this->__atabel[$i][4]);
        }
        $tmp_out .= "</tr></thead>";
        $this->__tcolname = $tmp_out;
    }

    public function clear() {
        $this->__avalue = array();
        $this->__theader = "";
        $this->__tcolname = "";
    }

    public function set_header($tlapor="", $tperiod="") {
        $thasil = '<div style="text-align:center;font-size:12pt;"><b>'.strtoupper($_SESSION['__WEB_APP']['REG_NAME']);
        $thasil .= (($tlapor=="") ? "" : '<br>'.$tlapor) . '</b>';
        $thasil .= ($tperiod=="") ? "" : '<br><span style="font-size:11pt;">'.$tperiod.'</span>';
        $thasil .= '</div><br>';
        $this->__theader = $thasil;
    }

    public function add_value() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0];
        $tmp_out = "<tr>";
        for ($i=0; $i<count($this->__atabel); $i++) {
            $tmp_val = $tmp_arr[$i];
            if (is_array($this->__atabel[$i][2]) && trim($tmp_val)<>"") {
                $tmp_val = $this->__atabel[$i][2][$tmp_val];
            } else if (function_exists($this->__atabel[$i][2]) && trim($tmp_val)<>"") {
                $tmp_val = call_user_func($this->__atabel[$i][2], $tmp_val);
            }
            $tmp_val = $this->set_align($tmp_val, $this->__atabel[$i][1], $this->__atabel[$i][3]);
            $tmp_out .= $tmp_val;
        }
        $tmp_out .= "</tr>";
        $this->__avalue[] = $tmp_out;
    }

    public function add_span() {
        $tmp_arr = func_get_args();
        $tmp_out = "<tr>";
        for ($i=0; $i<count($tmp_arr); $i++) {
            $tmp_val = $tmp_arr[$i];// CONTENT, COLSPAN=1, L, FUNC
            if (function_exists($tmp_val[3]) && trim($tmp_val[0])<>"") {
                $tmp_val[0] = call_user_func($tmp_val[3], $tmp_val[0]);
            }
            $tmp_val = $this->set_align($tmp_val[0], 0, $tmp_val[2], false, false, $tmp_val[1]);
            $tmp_out .= $tmp_val;
        }
        $tmp_out .= "</tr>";
        $this->__avalue[] = $tmp_out;
    }

    public function add_empty() {
        $this->__avalue[] = $this->set_align("&nbsp;", 0, "L,NB", true);
    }

    public function add_stamp($ctk_ke=1, $bnama=false) {
        $this->__avalue[] = $this->set_align($_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):""), 0, "L,NB", true);
    }

    public function get_stamp($ctk_ke=1, $bnama=false) {
        return $_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):"");
    }

    public function createPDF($nama_file = "Nama_File.pdf", $nfontz = 8, $orien = "P") {
        $html = '
        <style>
        #pdf_area {
            width: 100%;
            color: #000;
        }
        #pdf_area p { margin: 0; padding: 0; }
        #pdf_cont table {
            border: 1px solid #ccc;
            border-spacing: 0;
            border-collapse: collapse; 
            overflow: auto;
        }
        #pdf_cont table th, #pdf_cont table td {
            font-size: 12pt;
            text-align: left;
        }
        #pdf_cont table th {
            text-align: center;
            font-weight: bold;
            padding: 2mm;
            border: 1px solid #ccc;
        }
        #pdf_cont table td {
            padding: 1mm;
        }
        #pdf_cont table td.left { text-align: left; }
        #pdf_cont table td.center { text-align: center; }
        #pdf_cont table td.right { text-align: right; }
        #pdf_cont table td.bold { font-weight: bold; }
        #pdf_cont table td.italic { font-style: italic; }
        </style>
        <body class="pdf_area">
        <htmlpagefooter name="RepFoot">
        <table width="100%" style="border:none;"><tr><td width="50%" style="text-align:left;border:none;">'.$this->get_stamp(1, true).'</td><td width="50%" style="text-align:right;font-style:italic;border:none;">Halaman : {PAGENO} dari {nbpg}</td></tr></table>
        </htmlpagefooter>
        <sethtmlpagefooter name="RepFoot" show-this-page="1" value="on" />';
        if ($this->__theader<>"") $html .= $this->__theader;
        $html .= '<div id="pdf_cont"><table>';
        if ($this->__tcolname<>"") $html .= $this->__tcolname;
        $html .= '<tbody>';

        // content out
        for ($i=0; $i<count($this->__avalue); $i++) {
            $html .= $this->__avalue[$i];
        }
        $html .= '</tbody></table></div></div></body>';
        $mpdf = new mPDF('c', 'A4'.($orien=="L"?"-L":""), $nfontz, '', 5, 5, 5, 10, 5, 5);
        $mpdf->useSubstitutions = false;
        $mpdf->WriteHTML($html);
        $mpdf->Output($nama_file.".pdf", "I");
    }

    public function set_align($ttext, $nlen, $tdir="L", $bline=false, $bhead=false, $ncol=1) {
        $tstyle = $this->create_style($tdir);
        if (strpos(strtolower($ttext), "<tr>")===false) { //jika tidak ada <tr> up 2011-04-14
            if ($bline===true) {
                return '<tr><td colspan="'.count($this->__atabel).'" class="'.$tstyle.'">'.$ttext."</td></tr>";
            } else {
                return (($bhead===true)?"<th":"<td").(($ncol>1)?' colspan="'.$ncol.'"':"").' class="'.$tstyle.'" '.(($bhead===true)?'width="'.$nlen.'"':"").">".$ttext.(($bhead===true)?"</th>":"</td>");
            }
        } else {
            return (($bline===true) ? $ttext : (($bhead===true)?"<th":"<td").(($ncol>1)?" colspan=\"".$ncol."\"":"")." class=\"".$tstyle." \" ".(($bhead===true)?"width=\"".$nlen."\"":"").">".$ttext.(($bhead===true)?"</th>":"</td>"));
        }
    }

    private function create_style($tstyle="L") {
        $apad = array("L"=>"left", "R"=>"right", "C"=>"center", "NB"=>"nb", "NL"=>"noleft", "NR"=>"noright", "B"=>"bold", "I"=>"italic");
        $_astyle = explode(",", $tstyle);
        $thasil = "";
        foreach ($_astyle as $_key => $_val) {
            $thasil .= " ".$apad[$_val];
        }
        return substr($thasil,1);
    }
}

?>
