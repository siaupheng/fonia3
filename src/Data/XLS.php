<?php

/*******************************************************************

    Module        : /Data/XLS.php
    Desc.         : v3 - Class pembuatan file XLS
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : November 18th, 2012.
    Last Modified : October 26th, 2023.

    (c) 2012 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Data;

use PhpOffice\PhpSpreadsheet\IOFactory;


final class XLS {
    private $__tnamafile = null;
    private $__bline = true;
    private $__wbook = null;
    private $__wsheet = null;
    private $__baris = 0;
    private $__kolom = 0;
	private $__atabel = [];
    private $__astyle =[];
    private $__aheader = [];
    private $__fhead = null;
    private $__isheet = 0;

    public function __construct($tnama_file="", $oprop=[]) {//oke
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );
        $this->__tnamafile = $tnama_file;
        $this->__wbook = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $this->__wbook->getProperties()
            ->setCreator($_SESSION['__WEB_APP']['APP_NAME']." v".$_SESSION['__WEB_APP']['APP_VER'])
            ->setLastModifiedBy($_SESSION['__WEB_APP']['APP_NAME']." v".$_SESSION['__WEB_APP']['APP_VER'])
            ->setTitle($oprop['title'] ?? "")
            ->setSubject($oprop['subject'] ?? "")
            ->setDescription($oprop['desc'] ?? "")
            ->setCompany($_SESSION['__WEB_APP']['REG_NAME']);
        $this->__wsheet = $this->__wbook->getActiveSheet();
        $this->__fhead = [
            'font' => ['bold' => true],
            'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 'wrapText' => true],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['rgb'=>"D0D0D0"]],
            'borders' => ['allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['rgb'=>"C0C0C0"]]]
        ]
    }

    public function create_style($tstyle="L") {//oke
        $_astyle = explode(",", $tstyle);
        $_tmp_style = [];
        $_bline = $this->__bline;
        foreach ($_astyle as $_key => $_val) {
            if ($_val == "L") $_tmp_style['alignment'] = ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT];
            if ($_val == "R") $_tmp_style['alignment'] = ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT];
            if ($_val == "C") $_tmp_style['alignment'] = ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER];
            if ($_val == "W") $_tmp_style['alignment'] = ['wrapText' => true];
            if ($_val == "B") $_tmp_style['font'] = ['bold' => true];
            if ($_val == "S") $_tmp_style['numberFormat'] = ['formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT];
            if ($_val == "N") $_tmp_style['numberFormat'] = ['formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2];
            if ($_val == "N0") $_tmp_style['numberFormat'] = ['formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2];
            if ($_val == "NL") $_bline = false;
            if ($_val == "H") {
                $_bline = false;
                $_tmp_style = $this->__fhead;
            }
        }
        return $_tmp_style;
    }

    public function clear_field() {//up : 2014-06-19
        $this->__atabel = [];
        $this->__astyle = [];
    }

    public function set_pos($nbaris=0, $nkolom=0) {
        $this->__baris = $nbaris;
        $this->__kolom = $nkolom;
    }

    public function set_garis_bawah($bgaris=true) {}//todo: deleted

    public function add_header($tjudul="") {//oke
        if ($tjudul=="") return;
        $this->__aheader[] = $tjudul;
    }

    public function add_empty_row($nrow=1) {//oke
        $this->__baris += $nrow;
    }

    public function add_field($tnama="", $nlebar=0, $talign="L", $ffunc="") {//oke
        $this->__atabel[] = [$tnama, $nlebar, $ffunc];
        $this->__astyle[] = $this->create_style($talign);
    }

    public function add_sheet($tnama_sheet="BARU", $bw_header=true) {//not yet !!!
        if ($this->__isheet > 0) {
            $this->__wbook->createSheet();
            $this->__wsheet = $this->__wbook->setActiveSheetIndex(++$this->__isheet);
        }
        $this->__wsheet->setTitle($tnama_sheet);
        for ($i=0; $i<count($this->__atabel); $i++) {
        //sampai disini dulu !!
            $this->__wsheet->getColumnDimension($this->__kolom+$i)->setWidth($this->__atabel[$i][1]);
        }
        $_fjudul = $this->__wbook->addFormat();
        $_fjudul->setBold();
        $_fjudul->setAlign("center");
        $_jkolom = count($this->__atabel);
        if ($_jkolom>0) $_jkolom--;
        foreach ($this->__aheader as $_key => $_val) {
            $this->__wsheet->write($this->__baris,$this->__kolom,$_val,$_fjudul);
            $this->__wsheet->setMerge($this->__baris,$this->__kolom,$this->__baris,$this->__kolom+$_jkolom);
            $this->__baris++;
        }
        $this->__baris++;
        if ($bw_header==true) $this->show_headers();
    }

	public function add_value() {
		$tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0];
		$tmp_out = "";
		for ($i=0; $i<count($this->__atabel); $i++) {
			$tmp_val = $tmp_arr[$i];
            if (is_array($this->__atabel[$i][1]) && trim($tmp_val)<>"") {
                $tmp_val = $this->__atabel[$i][1][$tmp_val];
			} else if (function_exists($this->__atabel[$i][1]) && trim($tmp_val)<>"") {
				$tmp_val = call_user_func($this->__atabel[$i][1], $tmp_val);
			}
			$tmp_out .= DELI_CSV . $tmp_val;
		}
		$tmp_out = substr($tmp_out, 1);
		$this->__avalue[] = $tmp_out;
	}

    public function send($tnama_file="File_Excel") {//oke
        $this->__wsheet->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$tnama_file.'"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($this->__wsheet, 'Xlsx');
        $writer->save('php://output');
    }

}

?>
