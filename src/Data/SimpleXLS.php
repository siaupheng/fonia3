<?php

/*******************************************************************

    Module        : /Data/SimpleXLS.php
    Desc.         : v3 - Class pembuatan file Simple XLS
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : November 18th, 2012.
    Last Modified : November 18th, 2023.

    (c) 2012 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Data;

final class SimpleXLS {
    private $__wbook = null;
    private $__atabel = [];
    private $__astyle = [];
    private $__aheader = [];
    private $__amerge = [];
    private $__avalue = [];
    private $__tsheet = null;

    public function __construct($oprop=[]) {
        $this->__wbook = new \Shuchkin\SimpleXLSXGen();
        $this->__wbook
            ->setAuthor($_SESSION['__WEB_APP']['APP_NAME']." v".$_SESSION['__WEB_APP']['APP_VER'])
            ->setLastModifiedBy($_SESSION['__WEB_APP']['APP_NAME']." v".$_SESSION['__WEB_APP']['APP_VER'])
            ->setTitle($oprop['title'] ?? "")
            ->setSubject($oprop['subject'] ?? "")
            ->setDescription($oprop['desc'] ?? "")
            ->setCompany($_SESSION['__WEB_APP']['REG_NAME'])
            ->setDefaultFont("Calibri")
            ->setDefaultFontSize(11);
    }

    private function coord2cell($x, $y) {
        $c = '';
        for ($i = $x; $i >= 0; $i = ((int)($i / 26)) - 1) {
            $c = chr(ord('A') + $i % 26) . $c;
        }
        return $c . ($y + 1);
    }

    private function create_style($tstyle="L") {
        $_astyle = explode(",", $tstyle);
        $_s_style = ""; $_e_style = "";
        foreach ($_astyle as $_key => $_val) {
            if ($_val == "L") {$_s_style .= '<left>'; $_e_style .= '</left>';}
            if ($_val == "R") {$_s_style .= '<right>'; $_e_style .= '</right>';}
            if ($_val == "C") {$_s_style .= '<center>'; $_e_style .= '</center>';}
            if ($_val == "W") {$_s_style .= '<wraptext>'; $_e_style .= '</wraptext>';}
            if ($_val == "B") {$_s_style .= '<b>'; $_e_style .= '</b>';}
            if ($_val == "I") {$_s_style .= '<i>'; $_e_style .= '</i>';}
            if ($_val == "S") {$_s_style .= "\0"; $_e_style .= '';}
            if ($_val == "N") {$_s_style .= '<style nf="#,##0.00">'; $_e_style .= '</style>';}
            if ($_val == "N0") {$_s_style .= '<style nf="#,##0">'; $_e_style .= '</style>';}
        }
        return ['s'=>$_s_style, 'e'=>$_e_style];
    }

    public function clear_field() {
        $this->__atabel = [];
    }

    public function add_header($tjudul="") {
        if ($tjudul=="") return;
        $this->__aheader[] = $tjudul;
    }

    public function add_empty_row($nrow=1) {
        $_jkolom = count($this->__atabel);
        for ($i=0; $i<$nrow; $i++) {
            $this->__avalue[] = array_fill(0,$_jkolom,"");
        }
    }

    public function add_field($tnama="", $nlebar=0, $talign="L", $ffunc="") {
        $this->__atabel[] = [$tnama, $nlebar, $ffunc];
        $this->__astyle[] = $this->create_style($talign);
    }

    public function add_sheet($tnama_sheet="BARU", $bw_header=true) {
        $this->__tsheet = $tnama_sheet;
        $_jkolom = count($this->__atabel);
        foreach ($this->__aheader as $_key => $_val) {
            $a_out = ['<style font-size="12"><center>'.$_val."</center></style>"];
            $this->__amerge[] = $this->coord2cell(0, count($this->__avalue)) . ":" . $this->coord2cell($_jkolom-1, count($this->__avalue));
            $this->__avalue[] = $a_out;
        }
        if ($bw_header==true) $this->show_headers();
    }

    public function add_value() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0];
        $a_out = [];
        for ($i=0; $i<count($this->__atabel); $i++) {
            $tmp_val = $tmp_arr[$i];
            if (is_array($this->__atabel[$i][2]) && trim($tmp_val)<>"") {
                $tmp_val = $this->__atabel[$i][2][$tmp_val];
            } else if (function_exists($this->__atabel[$i][2]) && trim($tmp_val)<>"") {
                $tmp_val = call_user_func($this->__atabel[$i][2], $tmp_val);
            }
            $a_out[] = $this->__astyle[$i]['s'] . $tmp_val . $this->__astyle[$i]['e'];
        }
        $this->__avalue[] = $a_out;
    }

    public function add_span() {
        $tmp_arr = func_get_args();
        $a_out = [];
        for ($i=0; $i<count($tmp_arr); $i++) {
            $tmp_val = $tmp_arr[$i]; // CONTENT, COLSPAN=1, L, FUNC
            if (!isset($tmp_val[1])) $tmp_val[1] = 1;
            if (!isset($tmp_val[2])) $tmp_val[2] = "L";
            if (isset($tmp_val[3])) {
                if (function_exists($tmp_val[3]) && trim("".$tmp_val[0])<>"") {
                    $tmp_val[0] = call_user_func($tmp_val[3], $tmp_val[0]);
                }
            }
            $tmp_style = $this->create_style($tmp_val[2]);
            if ($tmp_val[1]>1) $this->__amerge[] = $this->coord2cell(count($a_out), count($this->__avalue)) . ":" . $this->coord2cell((count($a_out)+$tmp_val[1])-1, count($this->__avalue));
            $a_out[] = $tmp_style['s'] . $tmp_val[0] . $tmp_style['e'];
            for ($k=1; $k<$tmp_val[1]; $k++) $a_out[] = null;
        }
        $this->__avalue[] = $a_out;
    }

    public function add_stamp($ctk_ke=1, $bnama=false) {
        $this->__avalue[] = ["<i>".$_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):"")."</i>"];
    }

    public function get_stamp($ctk_ke=1, $bnama=false) {
        return $_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):"");
    }

    public function show_headers() {
        $a_out = [];
        for ($i=0; $i<count($this->__atabel); $i++) {
            $a_out[] = '<style bgcolor="#D0D0D0"><center>'.$this->__atabel[$i][0].'</center></style>';
        }
        $this->__avalue[] = $a_out;
    }

    public function send($tnama_file="File_Excel") {
        $this->__wbook->addSheet($this->__avalue, $this->__tsheet);
        for ($i=0; $i<count($this->__atabel); $i++) {
            $this->__wbook->setColWidth(($i+1), $this->__atabel[$i][1]);
        }
        for ($i=0; $i<count($this->__amerge); $i++) {
            $this->__wbook->mergeCells($this->__amerge[$i]);
        }
        $this->__wbook->downloadAs($tnama_file);
    }

}

?>
