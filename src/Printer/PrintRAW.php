<?php

/*******************************************************************

    Module        : /Printing/PrintRAW.php
    Desc.         : v4 - Class cetak RAW
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : June 15th, 2008.
    Last Modified : November 1st, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Printer;

class PrintRAW {
    private $_header = [];
    private $_avalue = [];
    private $_atabel = [];
    private $_ncont = 0;
    private $_maxchr = 40;
    private $_spacekol = "  ";
    private $_config = [
        'fontsize' => "01",
        'nvlogo' => false,
        'opendraw' => false,
        'opendrawbegin' => false,
        'cutpaper' => false ];

    public function __construct() {
    }

    public function set_opendrawer($bbegin = true) {
        $this->_config['opendraw'] = true;
        $this->_config['opendrawbegin'] = $bbegin;
    }

    public function set_cutpaper() {
        $this->_config['cutpaper'] = true;
    }

    public function set_maxchr($nmax = 0) {//up: 2015-03-22
        if ($nmax > 0) $this->_maxchr = $nmax;
    }

    public function set_fontsize($nsize = 1) {//up: 2015-09-30
        if ($nsize > 0) $this->_config['fontsize'] = str_pad($nsize, 2, "0", STR_PAD_LEFT);
         
    }

    public function set_printnv_logo() {//up: 2015-03-23
        $this->_config['nvlogo'] = true;
    }

    public function add_header($nvlogo = false, $extra = []) {//up: 2016-11-12
        $this->_config['nvlogo'] = $nvlogo;
        $this->_header[] = $_SESSION['__WEB_APP']['REG_NAME'];
        if (count($extra) > 0) {
            foreach ($extra as $_val) {
                $this->_header[] = $_val;
            }
        }
        $this->_header[] = $_SESSION['__WEB_APP']['REG_CITY'];
    }

    public function add_sheet() {//up: 20170321
        $this->_ncont++;
    }

    public function add_field($tnama="", $nlebar=0, $talign="L", $ffunc="") {
        $this->_atabel[] = [$tnama, $nlebar, $ffunc, $talign];
    }

    public function show_fields() {
        $tmp_out = "<b>";
        for ($i=0; $i<count($this->_atabel); $i++) {
            $tmp_out .= $this->set_align($this->_atabel[$i][0], $this->_atabel[$i][1], "C");
            if ($i < (count($this->_atabel)-1)) $tmp_out .= $this->_spacekol;
        }
        $tmp_out .= "</b>";
        $this->_avalue[$this->_ncont][] = "__LINE -";
        $this->_avalue[$this->_ncont][] = $tmp_out;
        $this->_avalue[$this->_ncont][] = "__LINE -";
    }

    public function add_value() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0]; //up 2009-09-27
        $tmp_out = "";
        for ($i=0; $i<count($this->_atabel); $i++) {
            $tmp_val = $tmp_arr[$i];
            if (is_array($this->_atabel[$i][2]) && trim($tmp_val)<>"") { //up 2010-05-28
                $tmp_val = $this->_atabel[$i][2][$tmp_val];
            } else if (function_exists($this->_atabel[$i][2]) && trim($tmp_val)<>"") {
                $tmp_val = call_user_func($this->_atabel[$i][2], $tmp_val);
            }
            $tmp_val = $this->set_align($tmp_val, $this->_atabel[$i][1], $this->_atabel[$i][3]);
            if ($i < (count($this->_atabel)-1)) $tmp_val .= $this->_spacekol;
            $tmp_out .= $tmp_val;
        }
        $this->_avalue[$this->_ncont][] = $tmp_out;
    }

    public function add($ttext = "") {
        $this->_avalue[$this->_ncont][] = $ttext;
    }

    public function add_line($tmp = "-") {
        $this->add("__LINE ".$tmp);
    }

    public function add_empty($nrep = 1) {
        for ($i=0;$i<$nrep;$i++) $this->add("");
    }

    public function add_stamp($ctk_ke=1, $bnama=false) {
        $this->add($_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):""));
    }

    private function _json_out() {//up: 2016-11-12
        $acontent = array();
        $nmax = $this->_maxchr;

        for ($s=0; $s<count($this->_avalue); $s++) {
            for ($i=0; $i<count($this->_avalue[$s]); $i++) {
                $tmp_str = $this->_avalue[$s][$i];
                if (strpos($tmp_str, "_LINE")>0) {
                    $tmp_arr = explode(" ", $tmp_str);
                    $tmp_str = str_pad($tmp_arr[1], $nmax, $tmp_arr[1]);
                }
                $acontent[$s][] = $tmp_str;
            }
        }

        $aout = array('type' => "escp");
        $aout['config']  = $this->_config;
        $aout['header']  = $this->_header;
        $aout['content'] = $acontent;
        send_json($aout);
    }

    public function show($jumlah_ctk=1) {
        $this->_config['times'] = $jumlah_ctk;
        $this->_json_out();
    }

    public function set_align($ttext, $nlen, $tdir="L") {
        $apad = array("L"=>STR_PAD_RIGHT, "R"=>STR_PAD_LEFT, "C"=>STR_PAD_BOTH);
        $ttext = (strlen($ttext)>$nlen) ? substr($ttext, 0, $nlen) : $ttext;
        return str_pad($ttext, $nlen, " ", $apad[$tdir]);
    }
}

?>
