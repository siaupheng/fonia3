<?php

/*******************************************************************

    Module        : /Printing/PrintPOS.php
    Desc.         : v4 - Class cetak POS
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : June 15th, 2008.
    Last Modified : November 1st, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Printer;

/* VERSI BARU DENGAN MULTI SHEET */
class PrintPOS {//up: 20180108
    private $_header = [];
    private $_avalue = [];
    private $_astyle = [];
    private $_ncont = 0;
    private $_maxchr = 40;
    private $_config = [
        'fontsize' => 1,
        'nvlogo' => false,
        'opendraw' => false,
        'opendrawbegin' => true,
        'cutpaper' => false,
        'logo' => null ];

    public function __construct() {
    }

    public function set_opendrawer($bbegin = true) {
        $this->_config['opendraw'] = true;
        $this->_config['opendrawbegin'] = $bbegin;
    }

    public function set_cutpaper() {
        $this->_config['cutpaper'] = true;
    }

    public function set_maxchr($nmax = 0) {//up: 2015-03-22
        if ($nmax > 0) $this->_maxchr = $nmax;
    }

    public function set_fontsize($nsize = 1) {//up: 2015-09-30
        if ($nsize > 0) $this->_config['fontsize'] = $nsize;
    }

    public function set_printnv_logo() {//up: 2015-03-23
        $this->_config['nvlogo'] = true;
    }

    public function set_header_logo($logo_file = "") {//up: 2019-01-08
        if ($logo_file=="") return;
        if (file_exists($_SESSION['__WEB_APP']['REAL_PATH']."assets/images/".$logo_file)) {
            $this->_config['logo'] = $_SESSION['__WEB_APP']['WEB_PATH']."assets/images/".$logo_file;
        }
    }

    public function add_header($nvlogo = false, $extra = array()) {//up: 2018-12-25
        $this->_config['nvlogo'] = $nvlogo;
        if ($nvlogo==false && $this->_config['logo']==null) $this->_header[] = $_SESSION['__WEB_APP']['REG_NAME'];
        if (count($extra) > 0) {
            foreach ($extra as $_val) {
                $this->_header[] = $_val;
            }
        }
        $this->_header[] = $_SESSION['__WEB_APP']['REG_CITY'];
    }

    public function add_sheet() {//up: 20170321
        $this->_ncont++;
    }

    public function add() {//up: 20180524
        $val_arr = func_get_args(); $ttext = ""; $tstyle = "L";
        if (is_array($val_arr[0])) {
            foreach($val_arr as $aval) {
                $ttext .= $this->set_align((isset($aval[0])?$aval[0]:""), (isset($aval[1])?$aval[1]:0), (isset($aval[2])?$aval[2]:$tstyle));
            }
        } else {
            $ttext = (isset($val_arr[0])) ? $val_arr[0] : "";
            $tstyle = (isset($val_arr[1])) ? $val_arr[1] : $tstyle;
        }
        $this->_avalue[$this->_ncont][] = $ttext;
        $this->_astyle[$this->_ncont][] = $tstyle;
    }

    public function addl($ttext = "") {
        $this->add($ttext);
    }

    public function addr($ttext = "") {
        $this->add($ttext, "R");
    }

    public function addc($ttext = "") {
        $this->add($ttext, "C");
    }

    public function add_line($tmp = "-") {
        $this->add("__LINE ".$tmp);
    }

    public function add_empty($nrep = 1) {
        for ($i=0;$i<$nrep;$i++) $this->add("");
    }

    public function add_stamp($ctk_ke=1, $bnama=false) {
        $this->add($_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):""));
    }

    private function _json_out() {//up: 2016-11-12
        $acontent = array();
        $nmax = $this->_maxchr;

        for ($s=0; $s<count($this->_avalue); $s++) {
            for ($i=0; $i<count($this->_avalue[$s]); $i++) {
                $tmp_str = $this->_avalue[$s][$i];
                if (strpos($tmp_str, "_LINE")>0) {
                    $tmp_arr = explode(" ", $tmp_str);
                    $tmp_str = str_pad($tmp_arr[1], $nmax, $tmp_arr[1]);
                }
                $acontent[$s][] = ($this->_astyle[$s][$i]=="X") ? $tmp_str : $this->set_align($tmp_str, $nmax, $this->_astyle[$s][$i]);
            }
        }

        $aout = array('type' => "escp");
        $aout['config']  = $this->_config;
        $aout['header']  = $this->_header;
        $aout['content'] = $acontent;
        send_json($aout);
    }

    public function show($jumlah_ctk=1) {
        $this->_config['times'] = $jumlah_ctk;
        $this->_json_out();
    }

    public function set_align($ttext, $nlen=0, $tdir="L") {//up: 20180524
        $apad = array("L"=>STR_PAD_RIGHT, "R"=>STR_PAD_LEFT, "C"=>STR_PAD_BOTH);
        $nlen = ($nlen==0) ? strlen($ttext) : $nlen;
        $ttext = (strlen($ttext)>$nlen) ? substr($ttext, 0, $nlen) : $ttext;
        return str_pad($ttext, $nlen, " ", $apad[$tdir]);
    }
}

?>
