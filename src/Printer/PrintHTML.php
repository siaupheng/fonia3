<?php

/*******************************************************************

    Module        : /Printing/PrintHTML.php
    Desc.         : v4 - Class cetak HTML
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : June 15th, 2008.
    Last Modified : November 1st, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Printer;

final class PrintHTML {
    private $__avalue = [];
    private $__atabel = [];
    private $__css = "table";
    private $__tstamp = "";
    
    public function __construct() {
    }

    public function set_css($style = "") {
        if ($style=="") return;
        $this->__css = $style;
    }

    public function add_field($tnama="", $nlebar=0, $talign="L", $ffunc="", $nkol=1) {//up 2012-03-07
        $this->__atabel[] = [$tnama, $nlebar, $ffunc, $talign, $nkol];
    }

    public function show_fields() {
        $tmp_out = "<tr>";
        for ($i=0; $i<count($this->__atabel); $i++) {
            if ($this->__atabel[$i][4]==0) continue;
            $tmp_out .= $this->set_align($this->__atabel[$i][0], $this->__atabel[$i][1], "C", false, true, $this->__atabel[$i][4]);
        }
        $tmp_out .= "</tr>";
        $this->__avalue[] = $tmp_out;
    }

    public function clear() {
        $this->__avalue = [];
    }

    public function add_value() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0]; //up 2009-09-27
        $tmp_out = "<tr>";
        for ($i=0; $i<count($this->__atabel); $i++) {
            $tmp_val = $tmp_arr[$i];
            if (is_array($this->__atabel[$i][2]) && trim("".$tmp_val)<>"") { //up 2010-05-28
                $tmp_val = $this->__atabel[$i][2][$tmp_val];
            } else if (function_exists($this->__atabel[$i][2]) && trim("".$tmp_val)<>"") {
                $tmp_val = call_user_func($this->__atabel[$i][2], $tmp_val);
            }
            $tmp_val = $this->set_align($tmp_val, $this->__atabel[$i][1], $this->__atabel[$i][3]);
            $tmp_out .= $tmp_val;
        }
        $tmp_out .= "</tr>";
        $this->__avalue[] = $tmp_out;
    }

    public function add_span() {
        $tmp_arr = func_get_args();
        $tmp_out = "<tr>";
        for ($i=0; $i<count($tmp_arr); $i++) {
            $tmp_val = $tmp_arr[$i];// CONTENT, COLSPAN=1, L, FUNC, EXTRA
            if (!isset($tmp_val[1])) $tmp_val[1] = 1;
            if (!isset($tmp_val[2])) $tmp_val[2] = "L";
            if (!isset($tmp_val[4])) $tmp_val[4] = "";
            if (isset($tmp_val[3])) {
                if (function_exists($tmp_val[3]) && trim("".$tmp_val[0])<>"") {
                    $tmp_val[0] = call_user_func($tmp_val[3], $tmp_val[0]);
                }
            }
            $tmp_val = $this->set_align($tmp_val[0], 0, $tmp_val[2], false, false, $tmp_val[1], $tmp_val[4]);
            $tmp_out .= $tmp_val;
        }
        $tmp_out .= "</tr>";
        $this->__avalue[] = $tmp_out;
    }

    public function add_stamp($ctk_ke=1, $bnama=false) {//up 2023-10-13
        $this->__tstamp = $this->get_stamp($ctk_ke, $bnama);
    }

    public function get_stamp($ctk_ke=1, $bnama=false) {//up 2012-10-11
        return $_SESSION['__WEB_APP']['APP_NAME']."v".$_SESSION['__WEB_APP']['APP_VER'].date("\@ymd.His")."-".user_id(3).(($bnama==true)?"/".strtoupper(user_user()):"").(($ctk_ke>0)?"#".isi_nol($ctk_ke,2):"");
    }

    public function show($jumlah_ctk=1) {//up 2012-10-11
        $out = "<table class=\"".$this->__css."\"><tbody>";

        // header out
        // not yet !!

        // content out
        for ($i=0; $i<count($this->__avalue); $i++) {
            $tmp_str = $this->__avalue[$i];
            if (strpos($tmp_str, "_LINE")>0) $tmp_str = "<hr />";
            $out .= $tmp_str;
        }
        $out .= "</tbody></table>";
        $out .= $this->__tstamp;

        for ($i=1; $i<=$jumlah_ctk; $i++) echo $out;
    }

    public function set_align($ttext, $nlen, $tdir="L", $bline=false, $bhead=false, $ncol=1, $textra="") {//up 2011-04-14
        $tstyle = $this->create_style($tdir);
        if ($bline===true) {
            return "<tr><td colspan=\"".count($this->__atabel)."\" class=\"".$tstyle." ".$textra."\">".$ttext."</td></tr>";
        } else {
            return (($bhead===true)?"<th":"<td").(($ncol>1)?" colspan=\"".$ncol."\"":"")." class=\"".$tstyle." ".$textra."\" ".(($bhead===true)?"width=\"".$nlen."\"":"").">".$ttext.(($bhead===true)?"</th>":"</td>");
        }
    }

    private function create_style($tstyle="L") { //up 2014-10-12
        // style = Left Right Center No-Border No-Left No-Right Bold Italic
        $apad = ["L"=>"l", "R"=>"r", "C"=>"c", "NB"=>"nb", "NL"=>"noleft", "NR"=>"noright", "B"=>"b", "I"=>"i"];
        $_astyle = explode(",", $tstyle);
        $thasil = "";
        foreach ($_astyle as $_key => $_val) {
            $thasil .= " ".$apad[$_val];
        }
        return substr($thasil,1);
    }

    public function add_pagebreak() {}//delete: not used anymore
}

?>
