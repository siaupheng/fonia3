<?php

/*******************************************************************

    Module        : /DB/MySQL.php
    Desc.         : v4 - Class database MySQL / MariaDB
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 2nd, 2007.
    Last Modified : October 19th, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\DB;

final class MySQL {
    private $cnn;
    private $rs = 0;
    private $rec = array();
    private $row = 0;
    private $halt_on_error = 1;  //1: halt w message, 2: not halt w message, 3: not halt no message
    private $auto_free = 1;      //1: free query automatically, 0: must call free method to free query
    private $db_config = null;
    private $err_num = 0;
    private $err_msg = '';
    private $debug_msg = 1;
    private $ischangingdb = false;

    public function __construct() {
        if (!is_array($this->db_config)) $this->setConfig();
        $this->debug_msg = ($_SERVER['SERVER_ADDR'] == "127.0.0.1") ? 1 : 0;
    }

    private function setConfig() {
        global $APP_DATA;
        if (!is_array($APP_DATA)) {
            if (file_exists($_SESSION['__WEB_APP']['REAL_PATH']."apps/cfg/mysql.php"))
                require_once($_SESSION['__WEB_APP']['REAL_PATH']."apps/cfg/mysql.php");
        }
        $this->db_config = $APP_DATA;
    }

    public function getConfig() {
        return $this->db_config;
    }

    function Connect() {
        if (!$this->cnn) {
            $this->cnn = @mysqli_connect($this->db_config['host'], $this->db_config['user'], $this->db_config['pwd'], $this->db_config['db'], intval($this->db_config['port']), $this->db_config['sock']);
            if (!$this->cnn) {
                $this->ErrHandler("Connection failed!");
                return 0;
            } else {
                @mysqli_query($this->cnn, "SET NAMES utf8");
                @mysqli_query($this->cnn, "SET CHARACTER SET utf8");
                @mysqli_query($this->cnn, "SET character_set_connection=utf8");
            }
        }
        return $this->cnn;
    }

    function Connection() {
        return $this->cnn;
    }

    function ResultSet() {
        return $this->rs;
    }

    function Free() {
        if (is_resource($this->rs)) @mysqli_free_result($this->rs);
        $this->rs = 0;
        $this->row = 0;
        $this->rec = array();
        $this->err_num = 0;
        $this->err_msg = '';
    }

    function SafeSQL($str) {
        if (!$this->cnn) $this->Connect();
        return mysqli_real_escape_string($this->cnn, strval($str));
    }

    function &Query($stqry = "", $dbname = "") {
        if ($stqry == "") return 0;
        if (!$this->Connect()) return 0;
        if ($dbname != "" || $this->ischangingdb) {
            if ($dbname != "") {
                if (!@mysqli_select_db($this->cnn, $dbname)) {
                    $this->ErrHandler("Cannot select database!");
                    return 0;
                }
                $this->ischangingdb = true;
            } else {
                if (!@mysqli_select_db($this->cnn, $this->db_config['db'])) {
                    $this->ErrHandler("Cannot select database!");
                    return 0;
                }
                $this->ischangingdb = false;
            }
        }
        if ($this->rs) $this->Free();
        $this->rs = @mysqli_query($this->cnn, $stqry);
        $this->row = 0;
        $this->err_msg = @mysqli_error($this->cnn);
        if (!$this->rs) $this->ErrHandler("Invalid SQL!: ".$stqry);
        return $this->rs;
    }

    function Next() {
        if (!$this->rs) {
            $this->ErrHandler("Cannot move cursor to next record!");
            return 0;
        }
        $this->rec = @mysqli_fetch_array($this->rs, MYSQLI_ASSOC);
        $this->row += 1;
        $this->err_msg = @mysqli_error($this->cnn);
        $result = is_array($this->rec);
        if (!$result && $this->auto_free) $this->Free();
        return $result;
    }

    function Reset() {
        @mysqli_data_seek($this->rs, 0);
    }

    function Seek($rownum = 0) {
        return @mysqli_data_seek($this->rs, $rownum);
    }

    function Row($name = "", $defval = "") {
        if (isset($this->rec[$name])) {
            return (($this->rec[$name] == "") ? $defval : $this->rec[$name]);
        } else {
            return (($defval != "") ? $defval : "");
        }
    }

    function RowNumeric($name = "", $defval = 0) {
        $defval = is_numeric($defval) ? $defval : 0;
        if (isset($this->rec[$name])) {
            if (is_numeric($this->rec[$name])) return $this->rec[$name];
            return $defval;
        } else return $defval;
    }

    function RowFloat($name = "", $defval = 0) {
        $defval = is_numeric($defval) ? $defval : 0;
        if (isset($this->rec[$name])) {
            if ($this->rec[$name]) $defval = $this->rec[$name];
        }
        return floatval($defval);
    }
    
    function AllRow() {
        return $this->rec;
    }

    function ValueArray($colname = "") {
        $currrow = $this->row;
        @mysqli_data_seek($this->rs, 0);
        $result = array();
        while ($__row = mysqli_fetch_array($this->rs))
            if (isset($__row[$colname])) array_push($result, $__row[$colname]);
        @mysqli_data_seek($this->rs, $currrow);
        return $result;
    }

    function ValueArrayFormater($colname = "", $formater = "") {
        $currrow = $this->row;
        $formater = $formater == ""?$formater = "return \$__row[\$colname];":"return ".str_replace("<value>", "\$__row[\$colname]", $formater).";";
        @mysqli_data_seek($this->rs, 0);
        $result = array();
        while ($__row = mysqli_fetch_array($this->rs))
            if (isset($__row[$colname])) array_push($result, eval($formater));
        @mysqli_data_seek($this->rs, $currrow);
        return $result;
    }

    function ValueList($colname = "", $delim = ",") {
        $result = @$this->ValueArray($colname);
        return @implode($delim, $result);
    }

    function CurrentRow() {
        return $this->row;
    }

    function AffectedRows() {
        return @mysqli_affected_rows($this->cnn);
    }

    function RecordCount() {
        return @mysqli_num_rows($this->rs);
    }

    function NumFields() {
        return @mysqli_num_fields($this->rs);
    }

    function LastInsertID() {
        $lastinsertid = -1;
        $crs  = @mysqli_query($this->cnn, "SELECT LAST_INSERT_ID() AS lastinsertid");
        $crow = @mysqli_fetch_array($crs);
        if (isset($crow["lastinsertid"])) if (is_numeric($crow["lastinsertid"])) $lastinsertid = $crow["lastinsertid"];
        return $lastinsertid;
    }

    function ErrHandler($err_msg = "") {
        $this->err_msg = @mysqli_error($this->cnn);
        if ($this->halt_on_error == 3) return;
        if ($err_msg == "") $err_msg = "Undefined error!";
        if ($this->debug_msg) {
            printf('<span style="font-size:1.2em;"><b>MySQL Error : %s</b> %s</span><br>', $this->db_config['db'], $this->err_msg);
            printf('<span style="font-size:1.2em;"><b>Database error : </b> %s</span><br>', $err_msg);
        }
        if ($this->halt_on_error == 1) die("");
    }

    function getDBName() {
        return $this->db_config['db'];
    }
}

?>
