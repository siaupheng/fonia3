<?php

/*******************************************************************

    Module        : /DB/SQL.php
    Desc.         : v4 - Class Query database
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 2nd, 2007.
    Last Modified : December 17th, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\DB;

final class SQL {
	private $__ttable = "";
	private $__afield = array();
	private $__avalue = array();
	private $__anomor = array();
	private $__twhere = "";
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia3\DB\MySQL(); }
	}

    public function SafeSQL($ttext = "") {
        return $this->__db->SafeSQL($ttext);
    }

	public function set_table($ttable = "") {
		$this->__ttable = $ttable;
	}

	public function add_param($tparam = "", $tvalue = null, $bnomor = false) {
		$this->__afield[] = $tparam;
		$this->__avalue[] = (is_null($tvalue) && isset($_REQUEST[$tparam])) ? $this->__db->SafeSQL($_REQUEST[$tparam]) : $tvalue;
		$this->__anomor[] = $bnomor;
	}

    public function add_param_date($tparam = "", $tvalue = null) {
        $this->__afield[] = $tparam;
        $this->__avalue[] = tgl_sql2str((is_null($tvalue) && isset($_REQUEST[$tparam])) ? $this->__db->SafeSQL($_REQUEST[$tparam]) : $tvalue);
        $this->__anomor[] = false;
    }

    public function add_param_stamp($tparam = "DateStamp") {
        $this->__afield[] = $tparam;
        $this->__avalue[] = "NOW()";
        $this->__anomor[] = true;
    }

    public function add_param_unix() {
        $this->__afield[] = "Unix";
        $this->__avalue[] = user_unix();
        $this->__anomor[] = false;
    }

    public function add_param_userid($tparam = "UserID") {
        $this->__afield[] = $tparam;
        $this->__avalue[] = user_id();
        $this->__anomor[] = false;
    }

    public function add_where_unix() {
        $this->__twhere .= (($this->__twhere=="") ? " WHERE " : " AND ") . "(Unix='".user_unix()."')";
    }

    public function add_where_date($tfield = "", $tvalue = null) {
        $tvalue = ($tvalue == null) ? "0000-00-00" : tgl_sql2str($tvalue);
        $this->add_where($tfield, $tvalue);
    }

    public function add_where($tfield = "", $tvalue = null) {
        $__tvalue = (is_null($tvalue) && isset($_REQUEST[$tfield])) ? $this->__db->SafeSQL($_REQUEST[$tfield]) : $tvalue;
        $this->__twhere .= (($this->__twhere=="") ? " WHERE " : " AND ") . "(".$tfield."='".$__tvalue."')";
    }

	public function clear_param() {
		$this->__afield = array();
		$this->__avalue = array();
		$this->__anomor = array();
	}

	public function clear_where() {
		$this->__twhere = "";
	}

	public function InsertDB() {
		$tsql = "INSERT INTO ".$this->__ttable." (";
		$tfield = "";
		$tvalue = "";
		for ($i=0; $i<count($this->__afield); $i++) {
			$tfield .= ",".$this->__afield[$i];
			$tvalue .= ",".(($this->__anomor[$i])?"":"'").$this->__avalue[$i].(($this->__anomor[$i])?"":"'");
		}
		$tsql .= substr($tfield,1);
		$tsql .= ") VALUES (";
		$tsql .= substr($tvalue,1);
		$tsql .= ")";
		$this->__db->Query($tsql);
	}

	public function UpdateDB() {
		$tsql = "UPDATE ".$this->__ttable." SET ";
		$tfield = "";
		for ($i=0; $i<count($this->__afield); $i++) {
			$tfield .= ",".$this->__afield[$i]."=".(($this->__anomor[$i])?"":"'").$this->__avalue[$i].(($this->__anomor[$i])?"":"'");
		}
		$tsql .= substr($tfield,1);
		$tsql .= ($this->__twhere=="") ? "" : $this->__twhere;
		$this->__db->Query($tsql);
	}

	public function DeleteDB() {
		$tsql = "DELETE FROM ".$this->__ttable;
		$tsql .= ($this->__twhere=="") ? "" : $this->__twhere;
		$this->__db->Query($tsql);
	}

    public function LastInsertID() {
        return $this->__db->LastInsertID();
    }
}

?>
