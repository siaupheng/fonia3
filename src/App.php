<?php

/*******************************************************************

    Module        : /App.php
    Desc.         : v3 - HTML5 Page Generator (jQuery & Bootstrap)
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : January 10th, 2008.
    Last Modified : January 21st, 2024.

    (c) 2008 - 2024, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3;

final class App {
    public  $version = "4.0.0";
    private $apps = array();
    private $auser = array();
    private $cache = null;
    private $menu = null;
    private $reg = null;
    protected $__db = null;
    protected $__form = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia3\DB\MySQL(); }
        $this->__form = new \siaupheng\fonia3\Web\Form($this->__db);

        $this->apps = $_SESSION['__WEB_APP'];
        if (isset($_SESSION['__WEB_USER'])) {
            $this->apps['TEMPLATE'] = "index.php";
            $this->auser = $_SESSION['__WEB_USER'];
            //$this->auser['user_info'] = tgl_sql2str($this->auser['Last_Date'], true) ." // ". $this->auser['Last_IP'].'<input type="hidden" id="Desktop_User_ID" value="'.intval($this->auser['UserID']).'"><input type="hidden" id="Desktop_User_Level" value="'.intval($this->auser['Level']).'"><input type="hidden" id="Desktop_User_UID" value="'.$this->auser['Username'].'">';
            $this->auser['user_img'] = "profile.png";
            if (file_exists($this->apps['REAL_PATH']."assets/images/profiles/".$this->auser['UserID'].".png")) {
                $this->auser['user_img'] = $this->auser['UserID'].".png";
            }
        }
        $this->reg = new \siaupheng\fonia3\Web\Register($this->__db);
        $this->cache = new \siaupheng\fonia3\Data\Cache($this->__db);
        $this->menu = new \siaupheng\fonia3\Web\Menu($this->__db);
    }

    public function TampilApp($aoption = array()) {
        // cek register
        if ($this->reg->reg_check()) {
            if (!user_logged()) {
                $this->apps['TEMPLATE'] = "login.php";
            }
        } else {
            $this->apps['TEMPLATE'] = "404.php";
        }

        // gunakan template khusus sesuai request
        if (($aoption['PAGE_404']['value'] ?? 0) == 1) {
            $this->apps['TEMPLATE'] = "404.php";
        }

        // loading template file
        $buffer = "";
        if (file_exists($this->apps['REAL_PATH']."assets/templates/".$this->apps['TEMPLATE'])) {
            $buffer = file_get_contents($this->apps['REAL_PATH']."assets/templates/".$this->apps['TEMPLATE']);
            $compos = 0;
            if (strpos(strtoupper($buffer), "<!DOCTYPE") !== false) {
                $compos = strpos(strtoupper($buffer), "<!DOCTYPE");
            }
            $buffer = substr($buffer, $compos);
        }

        // penambahan file css + js + app-js
        $headers = '<link rel="stylesheet" type="text/css" href="'.$this->cache->Main("css").'">'."\n";
        $headers .= '<script type="text/javascript" src="'.$this->cache->Main("js").'"></script>'."\n";
        if (user_logged()) $headers .= '<script type="text/javascript" src="'.$this->cache->Apps().'"></script>'."\n";

        $_secret = $this->apps['APP_CODE'] .";". $this->apps['APP_SID'] .";". $_SERVER['HTTP_HOST'] .";". $this->apps['WEB_PATH'] .";". "http://".$_SERVER['HTTP_HOST'].$this->apps['WEB_PATH'] .";". ($this->apps['AUTH_LEVEL']?$this->apps['AUTH_LEVEL']:0) .";". $this->version;
        $_suser = (user_logged()) ? $this->auser['Username'] .";". $this->auser['UserID'] .";". $this->auser['Grup_MenuID'] .";". $this->auser['Level'] : "nobody;999;999;9";
        $tnaviside = $tnavihead = "";
        $headers .= '<script type="text/javascript" language="javascript">'."\n";
        $headers .= 'FoniaJS.setConfig("'.base64_encode($_secret).'", "'.base64_encode($_suser).'");';
        $headers .= '$(document).ready(function(){';
        if (user_logged()) {
            $headers .= 'FoniaJS.loadMenu();';
            $tnaviside = $this->menu->Menu_Sidebar();
            $tnavihead = $this->menu->Menu_Header();
        }
        $headers .= '});';
        $headers .= '</script>'."\n";

        // informasi tahun aplikasi dan pembuatan menu
        if (strpos($buffer, '_CREATOR_YEAR_')===false) {
            $this->apps['REG_YEAR'] = ($this->apps['REG_YEAR']!=date('Y')) ? $this->apps['REG_YEAR']." - ".date('Y') : $this->apps['REG_YEAR'];
        } else {
            $this->apps['CREATOR_YEAR'] = ($this->apps['CREATOR_YEAR']!=date('Y')) ? $this->apps['CREATOR_YEAR']." - ".date('Y') : $this->apps['CREATOR_YEAR'];
        }

        // perubahan variabel dari data konfigurasi
        $asrc = array("[_CREATOR_NAME_]", "[_CREATOR_WEBSITE_]", "[_CREATOR_YEAR_]", "[_APP_NAME_VER_]", "[_APP_NAMES_VER_]", "[_APP_NAME_]", "[_APP_NAMES_]", "[_APP_NAMES_UP_]", "[_APP_VER_]", "[_APP_UPDATED_]", "[_REG_ID_]", "[_REG_NAME_]", "[_REG_NAME_UP_]", "[_REG_ADDRESS_]", "[_REG_CITY_]", "[_REG_CITY_UP_]", "[_REG_PHONE_]", "[_REG_FAX_]", "[_REG_EMAIL_]", "[_REG_POSTCODE_]", "[_REG_WEBSITE_]", "[_REG_YEAR_]", "[_USER_IMG_]", "[_WEB_PATH_]", "[_NAVI_SIDEBAR_]", "[_NAVI_HEADER_]");
        $adst = array($this->apps['CREATOR_NAME'], $this->apps['CREATOR_WEBSITE'], $this->apps['CREATOR_YEAR'], $this->apps['APP_NAME']." v".$this->apps['APP_VER'], $this->apps['APP_NAMES']." v".$this->apps['APP_VER'], $this->apps['APP_NAME'], $this->apps['APP_NAMES'], strtoupper($this->apps['APP_NAMES']??""), $this->apps['APP_VER'], $this->apps['APP_UPDATED'], $this->reg->reg_id(), $this->apps['REG_NAME'], strtoupper($this->apps['REG_NAME']??""), $this->apps['REG_ADDRESS'], $this->apps['REG_CITY'], strtoupper($this->apps['REG_CITY']??""), $this->apps['REG_PHONE'], $this->apps['REG_FAX'], $this->apps['REG_EMAIL'], $this->apps['REG_POSTCODE'], $this->apps['REG_WEBSITE'], $this->apps['REG_YEAR'], "profiles/".$this->auser['user_img'], $this->apps['WEB_PATH'], $tnaviside, $tnavihead);
        $buffer = str_replace($asrc, $adst, $buffer);

        if (isset($this->auser) && is_array($this->auser)) {
            $buffer = str_replace("[_USER_ID_]", $this->auser['UserID']??"", $buffer);
            $buffer = str_replace("[_USER_USER_]", $this->auser['Username']??"", $buffer);
            $buffer = str_replace("[_USER_NAME_]", $this->auser['Nama']??"", $buffer);
            $buffer = str_replace("[_USER_POSISI_]", $this->auser['Posisi']??"", $buffer);

            // menu modul
            $t_modul = "";
            if (isset($this->auser['MENU_MODUL']) && is_array($this->auser['MENU_MODUL']) && count($this->auser['MENU_MODUL'])>0) {
                $t_modul = '<div class="dropdown-divider"></div>';
                if ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
                    $href1 = $this->apps['WEB_PATH'].'../';
                    $href2 = "";
                } else {
                    $_prot = 'http';
                    $_host = $_SERVER['HTTP_HOST'];
                    if (isset($_SERVER['HTTPS'])) {
                        $_prot = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
                    }
                    $_prot .= "://";
                    if ($this->apps['PARENT_SUB']==true) {
                        $href1 = $_prot;
                        $href2 = ".". $_host;
                    } else {
                        $href1 = $_prot . $_host .'/';
                        $href2 = "";
                    }
                }
                foreach ($this->auser['MENU_MODUL'] as $modul) {
                    $href = $href1.$modul['APP_PATH'].$href2;
                    $href .= '/?auth='.$_SESSION['APP_TOKEN'];
                    $t_modul .= '<a class="dropdown-item slink" href="'.$href.'"><i class="icon me-2 '.$modul['Icon'].'"></i> '.$modul['Judul'].'</a>';
                }
            }
            // menu extra
            $amodul_ext = $aoption['MENU_MODUL'] ?? [];
            if (count($amodul_ext) > 0) {
                $t_modul .= '<div class="dropdown-divider"></div>';
                foreach ($amodul_ext as $modul) {
                    $t_modul .= '<a class="dropdown-item slink" id="'.$modul['Id'].'"><i class="icon me-2 '.$modul['Icon'].'"></i> '.$modul['Judul'].'</a>';
                }
            }
            $buffer = str_replace("[_MENU_MODUL_]", $t_modul, $buffer);
        }

        // penyisipan header
        $headpos = 0;
        if (strpos(strtolower($buffer), "</head>") !== false) {
            $headpos = strpos(strtolower($buffer), "</head>");
        }
        $split_1_buffer = substr($buffer, 0, $headpos);
        $split_2_buffer = substr($buffer, $headpos);
        $buffer = $split_1_buffer.$headers.$split_2_buffer;

        // penyisipan option (pilihan tahun, tanggal login, dll)
        if (count($aoption) > 0) {
            foreach ($aoption as $__key => $val) {
                if (strpos($__key, "OPTION")!==false) {
                    $opt_var = $this->__form->option("_", $__key, $val['value'], true, true, $val['option']);
                    $buffer = str_replace("[_".$__key."_]", $opt_var, $buffer);
                } else if (strpos($__key, "RADIO")!==false) {
                    $opt_var = $this->__form->radio("_", $__key, $val['value'], true, true, $val['radio']);
                    $buffer = str_replace("[_".$__key."_]", $opt_var, $buffer);
                } else if (strpos($__key, "PICKDATE")!==false) {
                    $txt_var = $this->__form->datebox("_", $__key, $val['value'], true, true);
                    $buffer = str_replace("[_".$__key."_]", $txt_var, $buffer);
                } else if (strpos($__key, "CUSTOM")!==false) {
                    $buffer = str_replace("[_".$__key."_]", $val['value'], $buffer);
                }
            }
        }

        echo $buffer;
    }
}

?>
