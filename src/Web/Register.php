<?php

/*******************************************************************

    Module        : /Web/Register.php
    Desc.         : v4 - Class Registrasi Aplikasi
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : November 23th, 2011.
    Last Modified : May 6th, 2021.

    (c) 2011 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

final class Register {
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia3\DB\MySQL(); }
    }

    static public function reg_id() {
        $id = $_SESSION['__WEB_APP']['REG_ID'] ?? "";
        return (substr($id,0,3).".".substr($id,3,3).".".substr($id,6));
    }

    static public function reg_code() {
        $id = $_SESSION['__WEB_APP']['REG_CODE'];
        return (substr($id,0,6)."-".substr($id,6,5)."-".substr($id,11,5)."-".substr($id,16,5)."-".substr($id,21,5)."-".substr($id,26,6));
    }

    public function reg_check() {
        $isreg = false;
        $this->__db->Query("SELECT UPPER(CONVERT(MD5(CONCAT(REG_YEAR,CONVERT(MD5(CONCAT(REG_ID,REG_NAME)) using latin1),REG_ID,REG_NAME)) using latin1)) KODE_REGISTER FROM app_config WHERE REG_ID='".$_SESSION['__WEB_APP']['APP_CODE']."' LIMIT 1");
        if ($this->__db->Next()) {
            if ($this->__db->Row("KODE_REGISTER","")==$_SESSION['__WEB_APP']['REG_CODE']) $isreg = true;
        }
        return $isreg;
    }
}

?>
