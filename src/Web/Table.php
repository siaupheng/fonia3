<?php

/*******************************************************************

    Module        : /Web/Table.php
    Desc.         : v4 - HTML Table (jQuery & Bootstrap)
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : January 16th, 2008.
    Last Modified : October 19th, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

final class Table {
    private $__ttable = "";
    private $__tfield = "";
    private $__twhere = "";
	private $__tquery = "";
    private $__tquery_footer = "";
	private $__aheader = [];
	private $__afield = [];
	private $__astyle = [];
	private $__aformat = [];
    private $__afooter = [];
	private $__aotype = [];
    private $__arowcol = [];
    private $__arow_func = [];
	private $__tfunc_select = "";
    private $__tfunc_navi = "";
    private $__btxtbutton = false;
	private $__bnobutton = false;
    private $__bscrollbar = false;
	private $__acontrol = ['field'=>false, 'value'=>""];
	private $__acbox = [];
    private $__aoption = [];
    private $__npage_rec = 10;
    private $__nreclimit = 0;
    private $__nscrollheight = 0;
    private $__ord_field = "";
    private $__ord_dir = "A";
    private $__tcss = "";
    private $__livedata = false;
    protected $__cdb = null;
    protected $__cpa = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__cdb = $db; }
            else { $this->__cdb = new \siaupheng\fonia3\DB\MySQL(); }
        $this->__cpa = new \siaupheng\fonia3\Web\Param();
	}
	
    public function set_table($ttable = "", $tfield = "", $twhere = "") {
        $this->__ttable = $ttable;
        if ($tfield != "") $this->__tfield = $tfield;
        if ($twhere != "") $this->__twhere = $twhere;
    }

    public function set_query($tquery = "") {
        $this->__tquery = $tquery;
    }

    public function set_query_footer($tquery = "") {
        $this->__tquery_footer = $tquery;
    }
	
    public function set_css($tcss = "") {
        $this->__tcss = $tcss;
    }

    public function set_limit($nlimit = 0) {
        $this->__nreclimit = $nlimit;
    }

    public function set_field_order($tfield = "", $torder = "A") {
        $this->__ord_field = $tfield;
        $this->__ord_dir = $torder;
    }

    public function set_textbtn($bistxt = true) {
        $this->__btxtbutton = $bistxt;
    }

    public function set_scrollbar($bisscroll = true, $nheight = 200) {
        $this->__bscrollbar = $bisscroll;
        $this->__nscrollheight = $nheight;
    }

    public function set_row_color($tfield = "", $tval = "", $tcss = "") {
        if ($tfield=="") return;
        $this->__arowcol[] = ['F'=>$tfield, 'V'=>$tval, 'C'=>$tcss];
    }

	public function set_func_navi($tfunc = "") {
		$this->__tfunc_navi = $tfunc;
	}

	public function set_func_select($tfunc = "") {
		$this->__tfunc_select = $tfunc;
	}

    public function set_func_btn($tfunc = "", $tlabel = "") {
        $this->__arow_func[] = ['label' => $tlabel, 'func' => $tfunc];
    }

	public function set_row_control($tfield = "", $tvalue = "Y") {
		$this->__acontrol['field'] = $tfield;
		$this->__acontrol['value'] = $tvalue;
	}

    public function add_row_form($tlabel = "", $tfunc = "", $tfield = "", $tvalue = "") {
        $this->__aoption[] = ['type' => "button", 'func' => $tfunc, 'icon' => "fon-tabel-dt", 'label' => $tlabel, 'field' => $tfield, 'value' => $tvalue];
    }

    public function add_row_btn($ticon = "", $tlabel = "", $tfunc = "", $tfield = "", $tvalue = "", $tfield_css = "", $avalue_css = []) {
        $this->__aoption[] = ['type' => "button", 'func' => $tfunc, 'icon' => $ticon, 'label' => $tlabel, 'field' => $tfield, 'value' => $tvalue, 'field_css' => $tfield_css, 'avalue_css' => $avalue_css];
    }

    public function add_row_del($tlabel = "", $tfunc = "", $tfield = "Unix", $tvalue = "") {
        $this->__aoption[] = ['type' => "button", 'func' => $tfunc, 'icon' => "x", 'label' => $tlabel, 'field' => $tfield, 'value' => $tvalue];
    }

	public function add_row_edit($tlabel = "", $tfunc = "") {
        $this->add_row_btn("note", $tlabel, $tfunc);
	}

	public function add_row_checkbox($tfield = "ID", $tcontrol = "Edit", $avalue = ["Y","N"]) {
		$this->__acbox['field'] = $tfield;
		$this->__acbox['control'] = $tcontrol;
		$this->__acbox['avalue'] = $avalue;
        $this->__aoption[] = ['type' => "checkbox", 'field' => $tfield, 'avalue' => $avalue];
	}

	public function no_button() {
		$this->__bnobutton = true;
	}

	public function add_field($tfield = "", $theader = "", $tstyle = "left", $tformat = "", $tfooter = "", $totype = "") {
		$this->__aheader[] = $theader;
		$this->__afield[] = $tfield;
		$this->__astyle[] = $tstyle;
		$this->__aformat[] = $tformat;
        $this->__afooter[] = $tfooter;
		$this->__aotype[] = $totype;
	}

    public function json_table($nama_tabel = "tabel_", $baris_tabel = 10) {
        $adata = []; $njml_kol = 0;

        // adding header
        for ($i=0; $i<count($this->__afield); $i++) {
            $adata['header'][] = ['f'=>$this->__astyle[$i], 'o'=>$this->__afield[$i], 'c'=>$this->__aheader[$i], 's'=>$this->__aotype[$i]];
            $njml_kol += ($this->__astyle[$i]=="hidden") ? 0 : 1;
        }
 
        // adding footer
        if ($this->__tquery_footer) {
            $this->__cdb->Query($this->__tquery_footer);
            $this->__cdb->Next();
            for ($i=0; $i<count($this->__afield); $i++) {
                $tval = $this->__cdb->Row($this->__afield[$i], 0);
                if (is_array($this->__afooter[$i])) {
                    $tval2 = $this->__afooter[$i][$tval];
                } else if (!is_null($this->__afooter[$i]) && function_exists($this->__afooter[$i])) {
                    $tval2 = call_user_func($this->__afooter[$i], $tval);
                } else {
                    $tval2 = ($this->__afooter[$i]<>"") ? $this->__afooter[$i] : $tval;
                }
                $adata['footer'][] = ['c'=>$tval2];
            }
        }

        $acontent = [];
        if (!$this->__livedata) {
            $this->__cdb->Query($this->__tquery);
            while ($this->__cdb->Next()) {
                $abaris = [];
                $aoption = ['button'=>[]];
                if (count($this->__aoption)>0) {
                    if ((($this->__acontrol['field']===false) || ($this->__cdb->Row($this->__acontrol['field'])==$this->__acontrol['value'])) && $this->__bnobutton===false) {
                        // adding all option
                        for ($i=0; $i<count($this->__aoption); $i++) {
                            if ($this->__aoption[$i]['type']=="button") {
                                if (($this->__aoption[$i]['field']!="") && ($this->__cdb->Row($this->__aoption[$i]['field'])<>(trim("".$this->__aoption[$i]['value'])!="" ? $this->__aoption[$i]['value'] : $_SESSION['__WEB_USER']['UNIX']))) {} else {
                                    $aoption['button'][] = [$this->__aoption[$i]['icon'], (empty($this->__aoption[$i]['field_css'])?'':$this->__aoption[$i]['avalue_css'][$this->__cdb->Row($this->__aoption[$i]['field_css'])]??'')];
                                }
                            }
                        }
                        // adding checkbox option
                        if (array_key_exists("field", $this->__acbox)) {
                            $aoption['checkbox']['field'] = $this->__cdb->Row($this->__acbox['field']);
                            $aoption['checkbox']['value'] = $this->__cdb->Row($this->__acbox['control']);
                        }
                    }
                }
                if (count($this->__arowcol)>0) {//up: 2014-06-13
                    for ($i=0; $i<count($this->__arowcol); $i++) {
                        if ($this->__cdb->Row($this->__arowcol[$i]['F']) == $this->__arowcol[$i]['V']) $aoption['rowcss'] = $this->__arowcol[$i]['C'];
                    }
                }
                $abaris[] = $aoption;
                for ($i=0; $i<count($this->__afield); $i++) {
                    $tval = stripslashes($this->__cdb->Row($this->__afield[$i]));//up: 2015-01-05
                    if (is_array($this->__aformat[$i])) {
                        $tval = $this->__aformat[$i][$tval];
                    } else if (!is_null($this->__aformat[$i]) && function_exists($this->__aformat[$i])) {
                        $tval = call_user_func($this->__aformat[$i], $tval);
                    }
                    $abaris[] = $tval;
                }
                $acontent[] = $abaris;
            }
        }

        // navigator
        $anavi = [ 'func_navi' => $this->__tfunc_navi, 'button' => $this->__arow_func ];

        $adata['type'] = "table";
        $adata['id'] = $nama_tabel;
        $adata['config'] = ['css' => $this->__tcss, 'row_count' => $baris_tabel, 'func_select' => $this->__tfunc_select, 'nobutton' => $this->__bnobutton, 'txtbutton' => $this->__btxtbutton, 'scrollbar' => $this->__bscrollbar, 'scroll_height' => $this->__nscrollheight, 'sort_col' => $this->__ord_field, 'sort_dir' => $this->__ord_dir, 'live' => $this->__livedata];
        $adata['option'] = [];
        foreach ($this->__aoption as $aval) {
            if ($aval['type']!=="checkbox") unset($aval['field']); unset($aval['value']); unset($aval['field_css']); unset($aval['value_css']); $adata['option'][] = $aval;
        }
        $adata['navi'] = $anavi;
        $adata['content'] = $acontent;
        return $adata;
    } 

    public function db_table($nama_tabel = "tabel_", $baris_tabel = 10) {
        $this->__livedata = true;
        $this->prepare_query();
        $id_draw = intval($this->__cpa->Get("draw", 0));
        $i_start = intval($this->__cpa->Get("start", 0));
        $i_length = intval($this->__cpa->Get("length", 0));
        $i_order = $this->__cpa->Get("order", 0);
 
        if ($id_draw > 0) {
            $record = 0;
            $this->__cdb->Query("SELECT COUNT(*) Item FROM ".$this->__ttable.$this->__twhere);
            if ($this->__cdb->Next()) $record = intval($this->__cdb->Row("Item", 0));
            $adata['draw'] = $id_draw;
            $adata['recordsTotal'] = $record;
            $adata['recordsFiltered'] = $record;

            $t_order = "ASC";
            if ($i_order <> 0) {
                $t_order = ($i_order[0]['dir']=="asc") ? "ASC" : "DESC";
                $i_order = $i_order[0]['column'];
            }
            if (($i_length > $record) || ($i_start >= $record) || ($record <= 10)) $i_length = -1;
            $sql_limit = ($i_start>=0 && $i_length != -1) ? " LIMIT ".$i_start.", ".$i_length : "";
            $sql_order = ($this->__afield[$i_order]) ? " ORDER BY ".$this->__afield[$i_order]." ".$t_order : "";

            $adata['data'] = [];
            $this->__cdb->Query("SELECT ".$this->__tfield." FROM ".$this->__ttable.$this->__twhere.$sql_order.$sql_limit);
            while ($this->__cdb->Next()) {
                $abaris = [];
                $aoption = ['button'=>[]];
                if (count($this->__aoption)>0) {
                    if ((($this->__acontrol['field']===false) || ($this->__cdb->Row($this->__acontrol['field'])==$this->__acontrol['value'])) && $this->__bnobutton===false) {
                        // adding all option
                        for ($i=0; $i<count($this->__aoption); $i++) {
                            if ($this->__aoption[$i]['type']=="button") {
                                if (($this->__aoption[$i]['field']!="") && ($this->__cdb->Row($this->__aoption[$i]['field'])<>(trim("".$this->__aoption[$i]['value'])!="" ? $this->__aoption[$i]['value'] : $_SESSION['__WEB_USER']['UNIX']))) {} else {
                                    $aoption['button'][] = [$this->__aoption[$i]['icon'], (empty($this->__aoption[$i]['field_css'])?'':$this->__aoption[$i]['avalue_css'][$this->__cdb->Row($this->__aoption[$i]['field_css'])]??'')];
                                }
                            }
                        }
                        // adding checkbox option
                        if (array_key_exists("field", $this->__acbox)) {
                            $aoption['checkbox']['field'] = $this->__cdb->Row($this->__acbox['field']);
                            $aoption['checkbox']['value'] = $this->__cdb->Row($this->__acbox['control']);
                        }
                    }
                }
                if (count($this->__arowcol)>0) {//up: 2014-06-13
                    for ($i=0; $i<count($this->__arowcol); $i++) {
                        if ($this->__cdb->Row($this->__arowcol[$i]['F']) == $this->__arowcol[$i]['V']) $aoption['rowcss'] = $this->__arowcol[$i]['C'];
                    }
                }
                $abaris[] = $aoption;
                for ($i=0; $i<count($this->__afield); $i++) {
                    $tval = stripslashes($this->__cdb->Row($this->__afield[$i]));//up: 2015-01-05
                    if (is_array($this->__aformat[$i])) {
                        $tval = $this->__aformat[$i][$tval];
                    } else if (!is_null($this->__aformat[$i]) && function_exists($this->__aformat[$i])) {
                        $tval = call_user_func($this->__aformat[$i], $tval);
                    }
                    $abaris[] = $tval;
                }
                $adata['data'][] = $abaris;
            }
        } else {
            $adata = $this->json_table($nama_tabel, $baris_tabel);
        }
        return $adata;
    }

    private function prepare_query() {
        if ($this->__tfield == "") $this->__tfield = "`".implode('`,`', $this->__afield)."`";
        if ($this->__twhere != "") $this->__twhere = " WHERE " . $this->__twhere;
    }
}

?>
