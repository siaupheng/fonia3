<?php

/*******************************************************************

    Module        : /Web/Terbilang.php
    Desc.         : v4 - Class Format Angka & Terbilang
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 12th, 2009.
    Last Modified : May 6th, 2021.

    (c) 2009 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

final class Terbilang {
    private $numb = array("","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan");

    public function __construct() {}

    public function Terbilang($nilai=0, $muang="Rupiah") {
        $number = 0;
        $hasil = "";
        $int = trim($nilai);
        if (is_int(strpos($int,"-"))) {
            $number = substr($int,strpos($int,"-")+1,strlen($int));
            $hasil = "Minus";
        } else {
            $number = $int;
        }

        # strpost : untuk mencari "." pada variabel number
        $xpos = strpos($number,".");
        if (is_int($xpos)) {
            # round : Untuk pembulatan angka desimal, strlen : untuk menghitung jumlah karakter
            $pecahan = round(substr($number,$xpos,strlen($number)),2);
            $last = substr($number,0,$xpos);
        } else {
            $pecahan = "";
            $last = $number;
        }
        if ($last==0 || $number==0) {
            return $hasil."NOL ".$muang;
        } else {
            # floor : Pembulatan angka terbaik dari desimal, pow : Ekspresi Eksponensial
            $triliun = floor($last/pow(10,12));
            $last = $this->mod($last,1000000000000);
             
            $miliar = floor($last/pow(10,9));
            $last = $this->mod($last,1000000000);
             
            $juta = floor($last/pow(10,6));
            $last = $this->mod($last,1000000);
             
            $ribu = floor($last/pow(10,3));
            $last = $this->mod($last,1000);
             
            $kata  = $this->tigadigit($triliun, "Triliun");
            $kata .= $this->tigadigit($miliar, "Miliar");
            $kata .= $this->tigadigit($juta, "Juta");
            $kata .= $this->tigadigit($ribu, "Ribu");
            $kata .= $this->tigadigit($last, "");
            $kata .= " ".$muang;
        }
        if ($pecahan>0) {
            $kata .= " Dan". $this->tigadigit(round($pecahan*100),"Sen");
        }
        return $hasil . $kata;
    }

    private function mod($a, $b) {
        return $a-$b*floor($a/$b);
    }

    private function tigadigit($amount, $suffix="") {
        $last = (int) $amount;
        $kata = "";
        if ($last == 1 && $suffix=="Ribu") {
            $kata = " Seribu";
            return $kata;
        }
        if ($last < 20 && $last > 10) {
            if ($last==11) {
                $kata = " Sebelas";
            } else {
                $kata = " ".$this->numb[$last-10]." Belas";
            }
            if ($suffix != "") {
                $kata .= " ". $suffix;
            }
            return $kata;
        }
        $ratus = floor($last/100);
        if ($ratus <= 0) {
            $kata .= "";
        } elseif ($ratus == 1) {
            $kata .= " Seratus";
        } else {
            $kata .= " ".$this->numb[$ratus]." Ratus";
        }
        $last = $this->mod($last,100);
        if ($last < 20 && $last > 10) {
            if ($last == 11) {
                $kata .= " Sebelas ". $suffix;
            } else {
                $kata .= " ".$this->numb[$last-10]." Belas ". $suffix;
            }
            return $kata;
        }
        $puluh = floor($last/10);
        if ($puluh == 0) {
            $kata .= "";
        } elseif ($puluh == 1) {
            $kata .= " Sepuluh";
        } else {
            $kata .= " ".$this->numb[$puluh]." Puluh";
        }
        $last = $this->mod($last,10);
        if ($last>0 && $last<=9) {
            $kata .= " ".$this->numb[$last];
        }
        if ($amount>0 && $amount<=1000) {
            $kata .= " ".$suffix;
        }
        return $kata;
    }
}

?>
