<?php

/*******************************************************************

    Module        : /Web/Button.php
    Desc.         : v3 - Button Generator (jQuery & Bootstrap)
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 27th, 2008.
    Last Modified : January 22nd, 2024.

    (c) 2008 - 2024, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

class Button {
    private $__abutton =[];

    public function __construct() {}
    private function add($type="info", $id="", $label="", $bmiss=false) {
        if ($id=="") $id = "random".rand(1000, 10000);
        $this->__abutton[] = ['type'=>$type, 'id'=>$id, 'label'=>$label, 'bmiss'=>$bmiss];
    }

    public function separator() {
        $this->__abutton[] = ['type'=>"separator"];
    }
    public function primary($id="", $label="", $bmiss=false) {
        $this->add("primary", $id, $label, $bmiss);
    }
    public function info($id="", $label="", $bmiss=false) {
        $this->add("info", $id, $label, $bmiss);
    }
    public function danger($id="", $label="", $bmiss=false) {
        $this->add("danger", $id, $label, $bmiss);
    }
    public function warning($id="", $label="", $bmiss=false) {
        $this->add("warning", $id, $label, $bmiss);
    }
    public function success($id="", $label="", $bmiss=false) {
        $this->add("success", $id, $label, $bmiss);
    }
    public function secondary($id="", $label="", $bmiss=false) {
        $this->add("secondary", $id, $label, $bmiss);
    }

    public function Button($size=2) {
        if (0==count($this->__abutton)) return "";
        $_out = '<div class="row mb-1"><div class="col-12 col-sm-6 my-0 mx-sm-0 text-center text-sm-start">'; $_ma = ' me-sm-2';
        foreach ($this->__abutton as $item) {
            if ("separator" == $item['type']) {
                $_out .= '</div><div class="col-12 col-sm-6 my-0 mx-sm-0 text-center text-sm-end">'; $_ma = ' ms-sm-2';
            } else {
                $_out .= '<button type="button" class="btn btn-'.$item['type'].' mb-2 mb-sm-0'.$_ma.' col-10 col-sm-auto shadow-none" id="'.$item['id'].'"'.(true==$item['bmiss']?' data-bs-dismiss="modal"':'').'>'.$item['label'].'</button>';
            }
        }
        $_out .= '</div></div>';
        return $_out;
    }
}

?>
