<?php

/*******************************************************************

    Module        : /Web/Menu.php
    Desc.         : v3 - Class menu
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : January 16th, 2008.
    Last Modified : December 14th, 2023.

    (c) 2008 - 2023, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

final class Menu {
    protected $__db = null;

    public function __construct($db = null){
        $this->__db = $db ?? new \siaupheng\fonia3\DB\MySQL();
    }

    private function Menu_Query($qry = "", $order = "Kode") {
	    $twhere = ($qry!="") ? "WHERE ".$qry : "";
	    $this->__db->Query("SELECT * FROM app_menu ".$twhere." ORDER BY ".$order." ASC");
	    $ahasil = array();
	    while ($this->__db->Next()) {
		    $ahasil[] = $this->__db->AllRow();
	    }
	    return $ahasil;
    }

    public function Menu_Sidebar() {//oke
        $a_modul = array();
        $is_modul = (($_SESSION['__WEB_APP']['APP_MODULE'] ?? "") <> "") ? true : false;
        if ($is_modul == true) {
            $a_modul[] = $_SESSION['__WEB_APP']['APP_MODULE'];
        } else {
            $atmp = $this->Menu_Query("Level=0 AND Aktif=1 AND Modul<>'IND'", "Urut");
            for ($i=0; $i<count($atmp); $i++) {
                $a_modul[] = $atmp[$i]['Modul'];
            }
        }

        $my_menu = array_unique(array_filter(explode(",", $_SESSION['__WEB_USER']['Menu_Grup'].",".$_SESSION['__WEB_USER']['Menu'])));
        $tsql = " AND Modul IN ('".implode("','",$a_modul)."')";
        $tsql .= ($is_modul == true) ? " AND Level>0" : "";
        $tsql .= " AND Kode IN ('".implode("','",$my_menu)."')";
    
        $amodul = $this->Menu_Query("Aktif=1".$tsql, "Kode");
        $thasil = '';
    
        $last_level = 0;
        for ($i=0; $i<count($amodul); $i++) {
            if (($i>0) and ($amodul[$i]['Level']<$amodul[$i-1]['Level'])) {
                $thasil .= str_repeat("</ul></li>", $amodul[$i-1]['Level']-$amodul[$i]['Level']);
            }
            $ticon = ($amodul[$i]['Icon']) ? '<i class="me-2 '.$amodul[$i]['Icon'].'"></i>' : "";
            if (($amodul[$i]['Level']>=0) && (isset($amodul[$i+1]) && ($amodul[$i+1]['Level']>$amodul[$i]['Level']))) {
                $thasil .= '<li class="nav-item has-submenu"><a class="nav-link" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'<i class="icon cui-caret-bottom float-end"></i></a><ul class="submenu collapse">';
            } else if (trim($amodul[$i]['Menu'])=="-") {
                $thasil .= '<li class="nav-divider"></li>';
            } else {
                $thasil .= '<li class="nav-item"><a class="nav-link mlink" id="'.$amodul[$i]['Kode'].'Link" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'</a></li>';
            }
            $last_level = $amodul[$i]['Level'];
        }
        if ($last_level>1) $thasil .= str_repeat("</ul></li>", $last_level-1);
        return $thasil;
    }

    public function Menu_Header() {//oke
        $a_modul = array();
        $is_modul = (($_SESSION['__WEB_APP']['APP_MODULE'] ?? "") <> "") ? true : false;
        $mulai_level = 0;
        if ($is_modul == true) {
            $a_modul[] = $_SESSION['__WEB_APP']['APP_MODULE'];
            $mulai_level = 1;
        } else {
            $atmp = $this->Menu_Query("Level=0 AND Aktif=1 AND Modul<>'IND'", "Urut");
            for ($i=0; $i<count($atmp); $i++) {
                $a_modul[] = $atmp[$i]['Modul'];
            }
        }

        $my_menu = array_unique(array_filter(explode(",", $_SESSION['__WEB_USER']['Menu_Grup'].",".$_SESSION['__WEB_USER']['Menu'])));
        $tsql = " AND Modul IN ('".implode("','",$a_modul)."')";
        $tsql .= ($is_modul == true) ? " AND Level>0" : "";
        $tsql .= " AND Kode IN ('".implode("','",$my_menu)."')";
    
        $amodul = $this->Menu_Query("Aktif=1".$tsql, "Kode");
        $thasil = '';
    
        $last_level = 0;
        for ($i=0; $i<count($amodul); $i++) {
            if (($i>0) and ($amodul[$i]['Level']<$amodul[$i-1]['Level'])) {
                $thasil .= str_repeat("</ul></li>", $amodul[$i-1]['Level']-$amodul[$i]['Level']);
            }
            $ticon = ($amodul[$i]['Icon']) ? '<i class="me-2 '.$amodul[$i]['Icon'].'"></i>' : "";
            if (isset($amodul[$i+1]) && ($amodul[$i+1]['Level']>$amodul[$i]['Level'])) {//have submenu
                if ($amodul[$i]['Level']==$mulai_level) {
                    $thasil .= '<li class="nav-item pe-4 dropdown"><a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'</a><ul class="dropdown-menu" data-bs-popper="none">';
                } else {
                    $thasil .= '<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle slink" data-bs-toggle="dropdown" aria-expanded="false" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'</a><ul class="dropdown-menu" data-bs-popper="none">';
                }
            } else if (trim($amodul[$i]['Menu'])=="-" && $amodul[$i]['Level']>$mulai_level) {//divider
                $thasil .= '<li class="dropdown-divider"></li>';
            } else if ($amodul[$i]['Level']==$mulai_level) {//level = 1 + no submenu
                $thasil .= '<li class="nav-item pe-4"><a class="nav-link mlink" id="'.$amodul[$i]['Kode'].'Link" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'</a></li>';
            } else {// level > 1 + no submenu
                $thasil .= '<li><a class="dropdown-item mlink" id="'.$amodul[$i]['Kode'].'Link" title="'.$amodul[$i]['Judul'].'">'.$ticon.$amodul[$i]['Menu'].'</a></li>';
            }
            $last_level = $amodul[$i]['Level'];
        }
        if ($last_level>$mulai_level) $thasil .= str_repeat("</ul></li>", $last_level-$mulai_level);
        return $thasil;
    }

    static function MenuJSON($db = null, $page = "", $tquery = "") {//oke
        $_mdb = $db ?? new \siaupheng\fonia3\DB\MySQL();
        $_query = ($tquery=="") ? "" : ",'".$tquery."'";
        $ajson = ['login' => true];
        $apage = ['Link' => ""];

        $acus_page = [
            'SYSP' => ['Kode'=>"SYSP", 'Judul'=>"Profil Pengguna", 'Link'=>"sys/profile.php", 'JS'=>"SYSProfileJS", 'Width'=>600, 'Button'=>'[{},{"i":"butBatal"},{"i":"butSave","v":"Simpan","c":"success"}]'],
            'SYSC' => ['Kode'=>"SYSC", 'Judul'=>"Ubah Password", 'Link'=>"sys/chpasswd.php", 'JS'=>"SYSChpasswdJS", 'Width'=>500, 'Button'=>'[{},{"i":"butBatal"},{"i":"butSave","v":"Simpan","c":"success"}]']
        ];

        if ($acus_page[$page]) {
            $apage = $acus_page[$page];
        } else if ($page <> "") {
            $_mdb->Query("SELECT * FROM app_menu WHERE Kode='".$page."'");
            if ($_mdb->Next()) $apage = $_mdb->AllRow();
            $apage['Width'] = intval($apage['Width']);
        }

        $ajson['iswindow'] = ($apage['Width'] > 0) ? true : false;
        $ajson['width'] = $apage['Width'];
        $ajson['title'] = $apage['Judul'];
        $ajson['button'] = is_json($apage['Button']) ? json_decode($apage['Button']) : $apage['Button'];
        $ajson['php'] = $apage['Link'];
        $ajson['func'] = $apage['JS'];
        $ajson['kode'] = $apage['Kode'];

        if (!file_exists($_SESSION['__WEB_APP']['REAL_PATH']."apps/php/".$apage['Link'])) $ajson['php'] = "";
        return $ajson;
    }
}

?>
