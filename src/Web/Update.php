<?php

/*******************************************************************

    Module        : /Web/Update.php
    Desc.         : v4 - Class Update Aplikasi Otomatis
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : June 14th, 2013.
    Last Modified : May 6th, 2021.

    (c) 2013 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia3\Web;

final class Update {
    private $__amysql = array();
    protected $__linkdb = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__linkdb = $db; }
            else { $this->__linkdb = new \siaupheng\fonia3\DB\MySQL(); }

        $this->__amysql = $this->__linkdb->getConfig();
    }

    public function check_update() {
        $_ainfo['build_cur'] = $_SESSION['__WEB_APP']['APP_BUILD'];
        $_ainfo['svr_status'] = "Server Online";
        $_read = ["","",""];

        $_url = "https://update.3fonia.com/".$_SESSION['__WEB_APP']['REG_ID']."/update.csv";
        $_fp = @fopen ($_url, 'r') or $_ainfo['svr_status'] = "Server Offline";
        if ($_fp) {
            $_read = fgetcsv ($_fp);
            fclose ($_fp);
        }

        $_ainfo['build_new'] = $_read[0];
        $_ainfo['file_web'] = $_read[1];
        $_ainfo['file_sql'] = $_read[2];
        return $_ainfo;
    }

    public function go_update($file_src="", $build_code="") {
        if ($file_src=="") return false;

        $_folder = $_SESSION['__WEB_APP']['REAL_PATH']."temp";
        if (!is_dir($_folder)) mkdir($_folder);

        $_dst = $_folder."/update.web.zip";
        if ($_dst) unlink($_dst);
        
        $new_data = file_get_contents($file_src);
        if (!$new_data) return false;

        $han_file = @fopen($_dst, 'w');
        if (!$han_file) return false;

        if ( fwrite($han_file, $new_data) ) {
            fclose($han_file);
            $zip = new ZipArchive();
            if ($zip->open($_dst) === true) {
                $zip->extractTo($_SESSION['__WEB_APP']['REAL_PATH']);
                $zip->close();
                $_SESSION['__WEB_APP']['APP_BUILD'] = $build_code;
                $this->__linkdb->Query("UPDATE app_config SET APP_BUILD='".$build_code."' WHERE REG_ID='".$_SESSION['__WEB_APP']['APP_CODE']."' LIMIT 1");
            }
        }
        if (file_exists($_dst)) unlink($_dst);
        return true;
    }

    public function update_mysql($file_sql="") {
        if ($file_sql=="") return false;

        $_folder = $_SESSION['__WEB_APP']['REAL_PATH']."temp";
        if (!is_dir($_folder)) mkdir($_folder);

        $_dst = $_folder."/update.sql.zip";
        if ($_dst) unlink($_dst);
        
        $new_data = file_get_contents($file_sql);
        if (!$new_data) return false;

        $han_file = @fopen($_dst, 'w');
        if (!$han_file) return false;

        if ( fwrite($han_file, $new_data) ) {
            fclose($han_file);
            $zip = new ZipArchive();
            if ($zip->open($_dst) === true) {
                $zip->extractTo($_folder);
                $zip->close();
            }
        }
        if (file_exists($_dst)) unlink($_dst);
        if (!is_readable($_folder."/update.sql")) return false;    

        $_link = @new mysqli($this->__amysql['host'], $this->__amysql['user'], $this->__amysql['pwd'], $this->__amysql['db'], $this->__amysql['port'], $this->__amysql['sock']);
        if (!$_link->connect_error) return false;

        $aSQL = file($_folder."/update.sql");
        foreach ($aSQL as $stmt) {
            if ($_link->query($stmt) === false) {
                $sqlECode = $_link->mysql_errno;
                $sqlEText = $_link->error;
                $sqlStmt = $stmt;
                break;
            }
        }
        if (file_exists($_folder."/update.sql")) unlink($_folder."/update.sql");
        if ($sqlECode == 0) {
            return true;
        } else {
            return false;
            /*echo "FAIL!<br/>";
            echo "Error code: $sqlErrorCode<br/>";
            echo "Error text: $sqlErrorText<br/>";
            echo "Statement:<br/> $sqlStmt<br/>";*/
        }
    }
}

?>
