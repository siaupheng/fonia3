/*******************************************************************

    Module        : /assets/js/pdf.js
    Desc.         : JS - Cetak Laporan PDF
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : April 6th, 2015.
    Last Modified : October 18th, 2020.

    (c) 2015 - 2020, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

var cPDF = function(url, options){
    var vars = {
        'url': '',
        'zoom': 80,
        'param': []
    };
    this.construct = function(url, options){
        $.extend(vars, options);
        vars.url = url;
    };
    this.set_url = function (url){
        vars.url = url;
    };
    this.set_zoom = function (zoom){
        if (zoom) vars.zoom = zoom;
    },
    this.add_param = function (par_id, par_val){
        var _getvalue = (par_val) ? par_id + "=" + escape(par_val) : par_id;
        vars.param.push(_getvalue);
    },
    this.downloadPDF = function(){
        if (!vars.url) {
            console.warn(vars.param + ", cek url php!");
            return false;
        }
        vars.data = FoniaJS.RequestParam(vars.param);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: (typeof vars.onSuccess === "function") ? vars.onSuccess : null,
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
        }).always(FoniaJS.FormDecorated);
        return this;
    };
    this.showPDF = function(){
        var _proto = window.location.protocol + '//';
        var _src = _proto + FoniaJS.WEB_HOST + vars.url + '?' + (FoniaJS.RequestParam(vars.param)).join('&') + '#zoom='+vars.zoom+',0,0&page=1';
        var _wtxt = 
            '<div class="modal fade" id="winAPPPDF" tabindex="-1" role="dialog" data-bs-keyboard="false" data-bs-show="false" data-bs-backdrop="static" aria-labelledby="winAPPPDFLabel" aria-hidden="true">'+
            '<div class="modal-dialog modal-dialog-centered modal-fs" role="document">'+
            '<div class="modal-content">'+
            '<div class="modal-header"><h5 class="modal-title" id="winAPPPDFLabel">Preview PDF</h5><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>'+
            '<div class="modal-body mb-0 p-0"><div class="ratio ratio-21x9"><object type="application/pdf" data="'+_src+'"><p>Tidak Mendukung Preview PDF</p></object></div></div>'+
            '</div></div></div>';
        if ($('#winAPPPDF')) $('#winAPPPDF').modal('dispose').remove();
        $(_wtxt).on('shown.bs.modal', function(e){
            FoniaJS.FormDecorated();
        }).on('hidden.bs.modal', function(e){
            $(this).remove();
        }).modal('show');
    };
    this.construct(url, options);
};
