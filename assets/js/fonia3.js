/*******************************************************************

    Module        : /assets/js/fonia3.js
    Desc.         : JS - Fonia Custom Class v3
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : June 19th, 2009.
    Last Modified : January 18th, 2024.

    (c) 2009 - 2024, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

'use strict';

var FoniaJS = {
    version: "3.0.0",
    APP_ID: 0,
    APP_SID: "",
    WEB_HOST: "",
    WEB_PATH: "",
    WEB_FULLPATH: "",
    AUTH_LEVEL: 9,
    User: {ID:null, UID:null, GID:0, Level:0},
    phpversion: "",
    locales: [],
    deflocal: "",
    _debug: (jQuery.inArray(location.hostname, ["localhost","127.0.0.1"]) !== -1) ? true : false,

    setConfig: function(key, skey) {//oke
        var _hasil, _adata = null;
        if (typeof(key) === 'string' && key !== '') {
            _hasil = $.base64.decode(key);
            _adata = _hasil.split(";");
            this.APP_ID = _adata[0];
            this.APP_SID = _adata[1];
            this.WEB_HOST = _adata[2];
            this.WEB_PATH = _adata[3];
            this.WEB_FULLPATH = _adata[4];
            this.AUTH_LEVEL = parseInt(_adata[5]);
            this.phpversion = _adata[6];
        }
        if (typeof(skey) === 'string' && skey !== '') {
            _hasil = $.base64.decode(skey);
            _adata = _hasil.split(";");
            this.User.ID = _adata[0];
            this.User.UID = parseInt(_adata[1]);
            this.User.GID = parseInt(_adata[2]);
            this.User.Level = parseInt(_adata[3]);
        }
    },
    user_level: function(l) {//oke
        if (this.User.Level <= (l ?? 9)) return true;
        return false;
    },
    user_level_not: function(l) {//oke
        if (this.User.Level !== (l ?? 9)) return true;
        return false;
    },
    callMenu: function(p, q) {//oke
        var apar = ["p=" + p];
        if (q != null) apar.push("q=" + q);
        new Request.JSON({
            url: this.WEB_PATH + "auth.php",
            sendParam: apar,
            onSuccess: function(apop) {
                if (apop.login == false) {
                    this.showErrorLocale("Fonia.sess_expire");
                    setTimeout(function(){self.location = FoniaJS.WEB_PATH;}, 1600);
                    return false;
                }
                if (apop.php == "" || !window[apop.func]) {
                    this.showErrorLocale("Fonia.page404");
                    return false;
                }
                window[apop.func]['PHP'] = this.WEB_PATH + "apps/php/" + apop.php;
                window[apop.func]['KODE'] = apop.kode;
                if (apop.iswindow == true) {
                    if (IndexJS.loading) IndexJS.loading();
                    new Request({
                        url: this.WEB_PATH + "apps/php/" + apop.php,
                        sendParam: ["data=default"],
                        onSuccess: function(txt) {
                            this.showWindow(apop.kode, apop.title, txt, apop.width, apop.button, function(){
                                if (typeof window[apop.func]['loading'] === 'function') window[apop.func]['loading'](q);
                            });
                        }.bind(this)
                    }).send();
                } else {
                    $("#pageToolbarBot").html(this.genToolButton(apop.button));
                    new Request({
                        url: this.WEB_PATH + "apps/php/" + apop.php,
                        sendParam: ["data=default"],
                        onSuccess: function(txt) {
                            $('#pageContent').html(txt);
                            if (typeof window[apop.func]['loading'] === 'function') window[apop.func]['loading'](q);
                        }
                    }).send();
                }
            }.bind(this)
        }).send();
    },

    loadMenu: function() {//oke
        $(".mlink").click(function(e){
            var sbar = $(this).closest('#sidebarMain');
            var linkid = $(this).attr('id').replace("Link", "");
            FoniaJS.callMenu(linkid);
            if (sbar.length > 0) { sbar.modal('toggle'); }
        });
        $(".slink").click(function(e){
            var ddown = $(this).closest('.dropdown-menu');
            if (ddown.length > 0) { ddown.dropdown('toggle'); }
        });
        if ($("#HomeLink").length){
            $("#HomeLink").click(function(){
                this.closeAllWindows();
                if (IndexJS.loading) IndexJS.loading();
            }.bind(this));
        }
        if ($("#ExitLink").length) {
            $("#ExitLink").click(function() {
                new Request.JSON({
                    url: this.WEB_PATH + "apps/php/login.php",
                    sendParam: ["mode=logout"],
                    onFailure: function(){
                        self.location = (['localhost','127.0.0.1'].indexOf(location.hostname) >= 0) ? this.WEB_PATH : "/";
                    }.bind(this),
                    onSuccess: function(respon){
                        var _nurl = (['localhost','127.0.0.1'].indexOf(location.hostname) >= 0) ? this.WEB_PATH : "/";
                        if (typeof respon !== 'undefined' && respon.nexthost !== '/') _nurl = respon.nexthost;
                        self.location = _nurl;
                    }.bind(this)
                }).send();
            }.bind(this));
        }
    },
    showInfo: function(message, wtime) {//oke
        var _tmpto = $('<div class="toast align-items-center text-white bg-info border-0" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="'+(wtime?wtime:3000)+'"><div class="d-flex"><div class="toast-body me-2">'+message+'</div><button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button></div></div>').appendTo('#div-toast');
        _tmpto.on('hidden.bs.toast', function(e){
            $(this).remove();
        });
        (new bootstrap.Toast(_tmpto)).show();
    },
    showError: function(message, wtime) {//oke
        var _tmpto = $('<div class="toast align-items-center text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="'+(wtime?wtime:3000)+'"><div class="d-flex"><div class="toast-body me-2">'+message+'</div><button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button></div></div>').appendTo('#div-toast');
        _tmpto.on('hidden.bs.toast', function(e){
            $(this).remove();
        });
        (new bootstrap.Toast(_tmpto)).show();
    },
    showInfoLocale: function(tobj, wtime) {
        this.showInfo(this.locale(tobj), wtime);
    },
    showErrorLocale: function(tobj, wtime) {
        this.showError(this.locale(tobj), wtime);
    },
    showAuth: function(fungcall, ocall, tmsg, bnotsuper, buserover) {
        var _breq = (ocall == null) ? false : true;
        var _bsuper = (bnotsuper == null) ? true : !bnotsuper;
        var _buover = (buserover == null) ? false : buserover;
        var _tsid = parseInt(Math.random() * 99999999);
        var _ceksuper = null;
        var _isi = new String('<div class="row mb-1" id="Auth_SP1"><div class="col-sm-6 col-lg-5">Username</div><div class="col-sm-6 col-lg-7"><input type="text" class="form-control" id="UID" name="UID" value="'+((_bsuper==false)?this.User.ID:'')+'" required="true"></div></div><div class="row mb-1" id="Auth_SP2"><div class="col-sm-6 col-lg-5">Password</div><div class="col-sm-6 col-lg-7"><input type="password" class="form-control" id="PWD" name="PWD" value="" required="true"></div></div><div class="row mb-1 d-none" id="Auth_SP3"><div class="col-12 text-center"><p>'+this.locale("Fonia.auth_wait_msg")+'</p></div></div><hr class="my-2"><div class="row mt-1"><div class="col-4"><button type="button" class="btn btn-outline-primary shadow-none" id="butSReq">Request</button></div><div class="col-8 text-end"><button type="button" class="btn btn-danger me-2" id="butSBatal" data-bs-dismiss="modal">Batal</button><button type="button" class="btn btn-success" id="butSOK">OK</button></div></div>');
        this.winSAuth = this.showWindow('SAuth', ((this.AUTH_LEVEL < 9 && _bsuper == true) ? this.locale("Fonia.auth_super") : this.locale("Fonia.auth_user")), _isi, 200, '', function(){
            $('#butSOK').click(function() {
                $('#butSOK').prop('disabled', true);
                new Request.JSON({
                    url: this.WEB_PATH + "auth.php",
                    sendParam: ["mode=auth", "data=super-check", "UID", "PWD", "SUPER=" + _bsuper, "UOVER=" + _buover],
                    onSuccess: function(adata) {
                        if (adata.status == 1) {
                            fungcall();
                        } else {
                            this.showError(adata.pesan);
                        }
                        this.closeWindow(this.winSAuth);
                    }.bind(this)
                }).send();
            }.bind(this));
            $('#butSBatal').click(function() {
                if (_ceksuper) {
                    clearInterval(_ceksuper);
                    new Request({
                        url: this.WEB_PATH + "auth.php",
                        sendParam: ["mode=auth", "data=super-cancel", "KODE=" + ocall.KODE, "REQ_SID=" + this.APP_SID, "REQ=" + _tsid]
                    }).send();
                }
            }.bind(this));
            $('#butSReq').click(function() {
                if (_breq == false) return false;
                $('#Auth_SP1').addClass('d-none');
                $('#Auth_SP2').addClass('d-none');
                $('#Auth_SP3').removeClass('d-none');
                $('#butSOK').prop('disabled', true);
                new Request({
                    url: this.WEB_PATH + "auth.php",
                    sendParam: ["mode=auth", "data=super-request", "KODE=" + ocall.KODE, "REQ_SID=" + this.APP_SID, "REQ=" + _tsid, "MSG=" + tmsg]
                }).send();
                var _cobalagi = 5;
                _ceksuper = setInterval(function() {
                    $('#butSBatal').html(this.locale("Fonia.cancel") + " (" + _cobalagi + ")");
                    if (_cobalagi == 0) {
                        clearInterval(_ceksuper);
                        new Request({
                            url: this.WEB_PATH + "auth.php",
                            sendParam: ["mode=auth", "data=super-cancel", "KODE=" + ocall.KODE, "REQ_SID=" + this.APP_SID, "REQ=" + _tsid]
                        }).send();
                        this.closeWindow(this.winSAuth);
                        this.showErrorLocale("Fonia.auth_fail");
                        return false;
                    }
                    new Request.JSON({
                        url: this.WEB_PATH + "auth.php",
                        sendParam: ["mode=auth", "data=request-check", "KODE=" + ocall.KODE, "REQ_SID=" + this.APP_SID, "REQ=" + _tsid],
                        onSuccess: function(adata) {
                            if (adata.status == 1) {
                                clearInterval(_ceksuper);
                                this.showInfoLocale("Fonia.auth_success");
                                fungcall();
                                this.closeWindow(this.winSAuth);
                            } else {
                                _cobalagi--;
                            }
                        }.bind(this)
                    }).send();
                }.bind(this), 3000);
            }.bind(this));
            $('#UID').keyup(function(e) {
                if (-1!==$.inArray(e.which, [13,9])) {
                    if (e.target.value != '') $('#PWD').focus();
                }
            });
            $('#PWD').keyup(function(e) {
                if (e.target.value == '') return false;
                if (13 == e.which) $('#butSOK').trigger('click');
            });
            if (_bsuper == false) {
                FoniaJS.setFocus("#PWD");
            } else {
                FoniaJS.setFocus("#UID");
            }
        }.bind(this));
    },
    genWinToolBut: function(menu) {//oke
        if (!menu) return '';
        var smenu = '';
        if (typeof menu === 'object'){
            var ma = ' me-sm-2'; smenu = '<div class="col-12 col-sm-6 my-0 mx-sm-0 text-center text-sm-start">';
            $.each(menu, function(i, atom) {
                if (Object.keys(atom).length === 0) {
                    smenu += '</div><div class="col-12 col-sm-6 my-0 mx-sm-0 text-center text-sm-end">';
                    ma = ' ms-sm-2';
                } else {
                    if (atom.i == 'butTutup') { atom.v='Tutup', atom.c='danger', atom.d='' };
                    if (atom.i == 'butBatal') { atom.v='Batal', atom.c='danger', atom.d='' };
                    var _xt = (typeof atom?.d !== 'undefined') ? ' data-bs-dismiss="modal"' : '';
                    smenu += '<button type="button" class="btn btn-'+(atom?.c ?? 'info')+' mb-2 mb-sm-0'+ma+' col-10 col-sm-auto" id="'+atom.i+'"'+_xt+'>' + atom.v + '</button>';
                }
            });
            smenu += '</div>';
            return smenu;
        } else {
            if (this._debug) console.log('menu not object'+menu);
        }
        return smenu;
    },
    genToolButton: function(menu) {//oke
        if (!menu) return '';
        var smenu = '';
        if (typeof menu === 'object'){
            var ma = ' me-sm-2'; smenu = '<div class="row mt-2 my-sm-2 justify-content-sm-between"><div class="col-sm-6 text-center text-sm-start">';
            $.each(menu, function(i, atom) {
                if (Object.keys(atom).length === 0) {
                    smenu += '</div><div class="col-sm-6 text-center text-sm-end">';
                    ma = ' ms-sm-2';
                } else {
                    if (atom.i == 'butTutup') { atom.v='Tutup', atom.c='danger', atom.d='' };
                    if (atom.i == 'butBatal') { atom.v='Batal', atom.c='danger', atom.d='' };
                    var _xt = "";//(typeof atom?.d !== 'undefined') ? ' data-bs-dismiss="modal"' : '';
                    smenu += '<button type="button" class="btn btn-'+(atom?.c ?? 'info')+' mb-2 mb-sm-0'+ma+' col-10 col-sm-3" id="'+atom.i+'"'+_xt+'>' + atom.v + '</button>';
                }
            });
            smenu += '</div></div>';
            return smenu;
        } else {
            if (this._debug) console.log('menu not object'+menu);
        }
        return smenu;
    },
    FormDecorated: function() {//oke
        $('input').each(function() {
            // 16:shift 17:ctrl 18:alt 35:end 36:home 37:left 38:up 39:right 40:down 9:tab 13:enter 27:esc
            // 8:backspace 46:delete 45:delete 48-57/96-105:num 110/190:dot 109/189/173:minus 107/187/61:plus
            if ($(this).hasClass('integer') || $(this).hasClass('angka')) {
                $(this).keydown(function(e) {
                    var key = e.keyCode ?? e.which;
                    return (jQuery.inArray(key,[8,9,13,46,110,190])!==-1 || (key>=16 && key<=18) || (key>=35 && key<=40) || (key>=48 && key<=57) || (key>=96 && key<=105));
                });
            } else if ($(this).hasClass('number')) {
                $(this).keydown(function(e) {
                    var key = e.keyCode ?? e.which;
                    return (jQuery.inArray(key,[8,9,13,46,110,190,109,189,173])!==-1 || (key>=16 && key<=18) || (key>=35 && key<=40) || (key>=48 && key<=57) || (key>=96 && key<=105));
                });
                $(this).focus(function() {
                    $(this).val( FoniaJS.StrToFloat($(this).val()) );
                });
                $(this).blur(function() {
                    $(this).val( FoniaJS.FloatToStr($(this).val(), 0) );
                });
            } else if ($(this).hasClass('double')) {
                $(this).keydown(function(e) {
                    var key = e.keyCode ?? e.which;
                    return (jQuery.inArray(key,[8,9,13,46,110,190,109,189,173])!==-1 || (key>=16 && key<=18) || (key>=35 && key<=40) || (key>=48 && key<=57) || (key>=96 && key<=105));
                });
                $(this).focus(function() {
                    $(this).val( FoniaJS.StrToFloat($(this).val()) );
                    $(this).select();
                });
                $(this).blur(function() {
                    $(this).val( FoniaJS.FloatToStr($(this).val(), 2) );
                });
            } else if ($(this).hasClass('phone')) {
                $(this).keyup(function() {
                    var key = e.keyCode ?? e.which;
                    return (jQuery.inArray(key,[8,9,13,46,110,190,109,189,173,107,187,61])!==-1 || (key>=16 && key<=18) || (key>=35 && key<=40) || (key>=48 && key<=57) || (key>=96 && key<=105));
                });
            } else if ($(this).hasClass('error') && $(this).val()) {
                var tmp_str = $(this).val();
                $(this).val('');
                FoniaJS.showError(tmp_str);
            }
        });
        $('.option-tags').each(function() {//oke
            if (!jQuery.isFunction(jQuery.fn.select2)) return;
            var _opts = {tags: false, dropdownAutoWidth: true, width: 'auto', theme: 'bootstrap-5'};
            var _dmodal = $(this).closest('div.modal');
            if ($(_dmodal).hasClass('modal')) _opts.dropdownParent = _dmodal;
            var _sel2 = $(this).select2(_opts);
            if ($(this).prop('required')) _sel2.on('select2:close', function(e){ $(this).valid(); });
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('.colorpic').each(function() {
            $(this).prop('background-color', $(this).val());
        });
        
        if('function' == typeof jQuery.fn.datepicker){
            $('.pickdate').datepicker({
                format: 'dd-mm-yyyy',
                weekStart: 1,
                language: 'id',
                autoclose: true,
                todayHighlight: true,
                enableOnReadonly: false
            });
        }
        if ('function' == typeof autosize) autosize($('textarea'));
    },

    RequestParam: function(aparam) {
        var tmp_hasil = [];
        var tmp_array = aparam;
        tmp_array.push('SID=' + parseInt(Math.random() * 99999999));
        $('input').each(function() {
            if (this.id.indexOf('_db')!==-1){
                tmp_array.push(this.id);
            }
            if (this.id.indexOf('_cbox')!==-1){
                tmp_array.push(this.id + '=' + this.value);
            }
        });
        $.each(tmp_array, function(i, nilai) {
            if (typeof nilai === "string") {
                var tmp_value = "";
                if (nilai.indexOf("=") == -1) {
                    if ($('#'+nilai).length) {
                        tmp_value = $('#'+nilai).val();
                        if (typeof tmp_value === 'undefined') {
                            tmp_value = ($('#'+nilai).is('.integer, .number, .double')) ? '0' : '';
                        } else {
                            tmp_value = ($('#'+nilai).is('.integer, .number, .double')) ? this.StrToFloat(tmp_value) : encodeURIComponent(tmp_value);
                        }
                        if ($('#'+nilai).hasClass("cgrpbox")) {
                            var _tmp_value = "";
                            $('input').each(function() {
                                if (this.id.indexOf(nilai + "_cgrpbox")!==-1  && this.checked) {
                                    _tmp_value += "," + this.value;
                                }
                            });
                            tmp_value = _tmp_value.substr(1);
                        }
                    }
                    tmp_value = nilai + "=" + tmp_value;
                } else {
                    tmp_value = nilai;
                }
                tmp_hasil.push(tmp_value);
            }
        }.bind(this));
        return tmp_hasil;
    },
    StrToFloat: function(str_val) {
        var nstr = str_val + "";
        var hasil = "";
        var fkali = 1;
        if (nstr.substr(0, 3) == "Rp.") {
            nstr = nstr.substr(3);
        }
        for (var i = 0; i < nstr.length; i++) {
            if ((parseFloat(nstr.charAt(i))) || (nstr.charAt(i) == "0") || (nstr.charAt(i) == ".")) {
                hasil += nstr.charAt(i);
            } else {
                if ((nstr.charAt(i) == "(") || (nstr.charAt(i) == ")") || (nstr.charAt(i) == "-")) {
                    fkali = -1;
                    hasil += "";
                } else {
                    if (nstr.charAt(i) == ",") {
                        hasil += "";
                    }
                }
            }
        }
        if (!hasil.indexOf(".") > 0) {
            hasil += ".00";
        }
        return parseFloat(hasil) * fkali;
    },
    FloatToStr: function(nangka, ndesi) {
        var nstr = nangka + "";
        ndesi = (ndesi == null) ? 0 : ndesi;
        nstr = (Math.round(nstr.replace(/\,/g, "") * Math.pow(10, ndesi)) / Math.pow(10, ndesi)) + "";
        var x = nstr.split(".");
        if (!x[0]) {
            x[0] = "0";
        }
        if (!x[1]) {
            x[1] = "";
        }
        var x1 = x[0];
        if (x1.length > 3) {
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "," + "$2");
            }
        }
        var x2 = x[1];
        if (x2.length < ndesi) {
            var g = x2;
            for (var i = x2.length + 1; i <= ndesi; i++) {
                g += "0";
            }
            x2 = g;
        }
        x2 = "." + x2;
        if (ndesi == 0) {
            x2 = "";
        }
        return x1 + x2;
    },
    formatBytes: function(bytes, deci) {//oke
        if (bytes === 0) return '0 Bytes';
        const dm = deci ?? 0;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(1024));
        return parseFloat((bytes / Math.pow(1024, i)).toFixed(dm)) + ' ' + sizes[i];
    },
    showConfirm: function(str_judul, str_isi, func_callback, conf_tipe) {//oke
        var _cfunc = (typeof func_callback == "function") ? func_callback : function() {};
        var _ctipe = (conf_tipe == null) ? 'modal-warning' : 'modal-'+conf_tipe;
        var _btipe = (conf_tipe == null) ? 'btn-warning' : 'btn-'+conf_tipe;
        var _wtxt = 
            '<div class="modal fade" id="winAPPConf" tabindex="-1" role="dialog" data-bs-keyboard="false" data-bs-show="false" data-bs-backdrop="false" aria-labelledby="winAPPConfLabel" aria-hidden="true">'+
            '<div class="modal-dialog modal-dialog-centered '+_ctipe+' modal-400" role="document">'+
            '<div class="modal-content">'+
            '<div class="modal-header"><h5 class="modal-title" id="winAPPConfLabel">'+str_judul+'</h5><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>'+
            '<div class="modal-body text-center mh-100">'+str_isi+'</div>'+
            '<div class="modal-footer"><button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button><button type="button" class="btn '+_btipe+' ms-2" id="winConfirmY">Ya</button></div>'+
            '</div></div></div>';
        if ($('#winAPPConf').length) $('#winAPPConf').modal('dispose').remove();
        $(_wtxt).on('shown.bs.modal', function(e){
            $('#winConfirmY').focus();
            $('#winConfirmY').click(function(e){
                _cfunc();
                $('#winAPPConf').modal('hide');
            });
        }).modal('show');
    },
    showWindow: function(id_wind, s_judul, s_isi, i_width, s_toolb, f_onload, b_scroll) {//oke
        var t_width = '', t_toolb = '', t_scroll = (typeof b_scroll == 'boolean' && b_scroll == false) ? '' : 'modal-dialog-scrollable';
        var f_oncall = (typeof f_onload == "function") ? f_onload : function(){};
        if (typeof i_width == 'string') { t_width = 'modal-'+i_width; } else
        if (i_width <= 300) { t_width = 'modal-sm'; } else
        if (i_width <= 400) { t_width = 'modal-400'; } else
        if (i_width <= 500) { t_width = 'modal-500'; } else
        if (i_width <= 800) { t_width = 'modal-lg'; } else
        if (i_width <= 1200) { t_width = 'modal-xl'; } else
        if (i_width > 1200) { t_width = 'modal-fs'; }
        if (typeof s_toolb !== 'undefined' && s_toolb !== '' && s_toolb !== null) {
            t_toolb = '<div class="modal-footer justify-content-sm-between">';
            t_toolb += this.genWinToolBut(s_toolb);
            t_toolb += '</div>';
        }
        var _wtxt = 
            '<div class="modal fade" id="win'+id_wind+'" tabindex="-1" data-bs-keyboard="false" data-bs-backdrop="static" aria-labelledby="win'+id_wind+'Label" aria-hidden="true">'+
            '<div class="modal-dialog modal-dialog-centered '+t_scroll+' '+t_width+'">'+
            '<div class="modal-content">'+
            '<div class="modal-header"><h5 class="modal-title" id="win'+id_wind+'Label">'+s_judul+'</h5><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>'+
            '<div class="modal-body">'+s_isi+'</div>'+ t_toolb +
            '</div></div></div>';
        if ($('#win'+id_wind).length) $('#win'+id_wind).modal('dispose').remove();
        $(_wtxt).on('shown.bs.modal', function(e){
            f_oncall();
            FoniaJS.FormDecorated();
        }).on('hidden.bs.modal', function(e){
            $(this).remove();
        }).modal('show');
        return '#win'+id_wind;
    },
    closeWindow: function(elWin) {//oke
        if ($(elWin).length) $(elWin).modal('hide');
    },
    closeAllWindows: function() {//oke
        $(".modal").each(function() {
            if ($(this).hasClass('show')) $(this).modal('toggle');
            if ($(this).attr('id')!=='sidebarMain') $(this).modal('dispose').remove();
        });
        $(".fon-autotext", ".fon-autopos").each(function() {
            $(this).remove();
        });
        if ($("#dataPrint").length) {
            $("#dataPrint").empty();
        }
    },
    setFocus: function(obj) {
        $(obj).select().focus();
    },
    lPad: function(n, width, z) {
        z = z || "0";
        n = n + "";
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    },
    setCookie: function(cname, cvalue, exdays) {//oke
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = 'f' + this.APP_ID + '_' + cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function(cname) {//oke
        var name = 'f' + this.APP_ID + '_' + cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    locale: function(key, oset){//oke
        if (typeof oset === "object") {
            this.locales[key] = oset;
            if (this.deflocal == '') this.deflocal = key;
        } else {
            if (!this.locales[this.deflocal]) return "";
            return key.split('.').reduce(function (cur, key) {
                return cur[key];
            }, this.locales[this.deflocal]);
        }
    },
    AutoComplete: function(elm, turl, aparam, func_comp, func_key){//oke
        var arrData = [], objComp = {};
        if ('string' !== typeof aparam) arrData = this.RequestParam(aparam);
        objComp = {
            //preventEnter: false,
            resolverSettings: {
                url: turl,
                extraData: arrData.reduce(function(o, val) { var t = val.split('='); o[t[0]] = t[1]; return o; }, {})
            }
        };
        if ('function' === typeof func_key) objComp.keydownFunc = func_key;
        if ('function' === typeof func_comp) objComp.selectFunc = func_comp;
        $(elm).autoComplete(objComp);
    },
    getAppID: function() {
        return this.APP_ID;
    }
};

var Request = function(options){
    var vars = {
        url: "",
        sendParam: [],
        onSuccess: null,
        onFailure: null,
        data: null
    };
    this.construct = function(options){
        $.extend(vars, options);
    };
    this.send = function(){
        if (!vars.url) {
            //if (this._debug)
            console.log(vars.sendParam + ", cek url php!");
            return false;
        }
        vars.data = FoniaJS.RequestParam(vars.sendParam);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: (typeof vars.onSuccess === "function") ? vars.onSuccess : null,
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
            complete: FoniaJS.FormDecorated,
        });
        return this;
    };
    this.construct(options);
};

Request.JSON = function(options){//oke
    var vars = {
        url: "",
        sendParam: [],
        onSuccess: null,
        onFailure: null,
        data: []
    };
    this.construct = function(options){
        $.extend(vars, options);
    };
    this.send = function(){
        if (!vars.url) {
            //if (this._debug)
            console.log(vars.sendParam + ", cek url php!");
            return false;
        }
        vars.data = FoniaJS.RequestParam(vars.sendParam);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: (typeof vars.onSuccess === "function") ? vars.onSuccess : null,
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
            complete: FoniaJS.FormDecorated,
            dataType: 'json'
        });
        return this;
    };
    this.construct(options);
};

Request.JSONF = function(options){//oke
    var vars = {
        url: "",
        sendParam: [],
        onSuccess: null,
        onFailure: null,
        data: null
    };
    this.construct = function(options){
        $.extend(vars, options);
    };
    this.send = function(){
        if (!vars.url) {
            //if (this._debug)
            console.log(vars.sendParam + ", cek url php!");
            return false;
        }
        vars.data = FoniaJS.RequestParam(vars.sendParam);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: function(data){
                if (data.length > 0) {
                    for (var no = 0; no < data.length; no++) {
                        var ini = data[no];
                        if (ini.type == "table" && $(ini.id)) make_table(ini);
                        if (ini.type == "form" && $(ini.id)) make_form(ini);
                        if (ini.type == "option" && $(ini.id)) make_option(ini);
                        if (ini.type == "html" && $(ini.id)) make_html(ini);
                        if (ini.type == "msg") show_msg(ini);
                    }
                }
                if (typeof vars.onSuccess === "function") vars.onSuccess(data);
            },
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
            complete: FoniaJS.FormDecorated,
            dataType: 'json'
        });
        return this;
    };

    var show_msg = function(ini){//oke
        var wtime = parseInt(ini.wait);
        if (ini.mode == "error") {
            FoniaJS.showError(ini.value, wtime);
        } else {
            FoniaJS.showInfo(ini.value, wtime);
        }
    };
    var make_html = function(ini){//oke
        $('#'+ini.id).html(ini.value);
    };
    var make_form = function(ini){//oke
        var formIni = $('#'+ini.id);
        formIni.val(ini.value);
        if (ini.css) {
            formIni.prop('disabled', ini.css=='disabled');
        }
    };
    var make_option = function(ini){//oke
        var optIni = $('#'+ini.id);
        /*if (ini.css) {
            optIni.prop('disabled', ini.css=='disabled');
        }*/
        if (!optIni) return;
        if (ini.avalue) {
            optIni.empty();
            var o_group = 0;
            $(ini.avalue).each(function(n, i) {
                if (i.optgroup) {
                    if (0 < o_group++) optIni.append('</optgroup>');
                    optIni.append('<optgroup label="'+i.optgroup+'">');
                    return;
                }
                optIni.append($('<option>', {
                    value: i.value, text: i.html || '\u00A0', label: i.text || '', 'olabel': i.label || ''
                }));
            });
            if (o_group > 0) optIni.append('</optgroup>');
        }
        if (ini.value) {
            optIni.val(ini.value);
        }
        optIni.trigger('change');
    };    
    var make_table = function(ini){//oke
        var divData = $('#'+ini.id).empty();
        var strHead = '<thead><tr>', iCol = 0, sClass = '', aSortCol = [], aTombol = [];
        $.each(ini.header, function(key, val){
            if (val.o == ini.config.sort_col) iCol = key;
            strHead += '<th data-class-name="'+val.f+'">'+val.c+'</th>';
            aSortCol.push((val.s ? { "orderDataType": val.s } : null));
        });
        if (ini.option.length > 0) {
            var strAksi = 'Aksi', ChkField = '';
            $.each(ini.option, function(key, val){
                if (val.type == 'checkbox'){
                    strAksi = '<input class="form-check-input" type="checkbox" id="'+ini.id+'_cbhead_'+val.field+'" value="'+val.avalue[1]+'">';
                    ChkField = val.field;
                }
                if (val.type == 'button') aTombol[val.icon] = val.label;
            });
            strHead += '<th data-class-name="c">'+strAksi+'</th>';
            aSortCol.push(null);
        }
        strHead += '</tr></thead>';

        var strFoot = '';
        if (ini.footer) {
            strFoot = '<tfoot><tr>';
            $.each(ini.footer, function(key, val){
                strFoot += '<th class="'+ini.header[key].f+'">'+val.c+'</th>';
            });
            if (ini.option.length > 0) {
                strFoot += '<th>&nbsp;</th>';
            }
            strFoot += '</tr></tfoot>';
        }

        var sClass = '';
        if (ini.config.css) sClass += ' '+ini.config.css;
        if (ini.config.func_select) sClass += ' table-select';
        var strTab = '<table id="'+ini.id+'Tabel" class="table table-striped table-bordered table-hover'+sClass+'">';

        var strBody = '<tbody>';
        $.each(ini.content, function(key, val){
            strBody += '<tr>';
            $.each(val, function(keyi, vali){
                if (keyi == 0) return;
                strBody += '<td>' + vali + '</td>';
            });
            if (ini.option.length > 0) {
                var _tbtn1 = "", _tbtn2 = "";
                if (val[0].rowcss) {
                    _tbtn1 += '<span class="rowcss '+val[0].rowcss+'"></span>';
                }
                if (val[0].button.length > 0) {
                    $.each(val[0].button, function(keyb, valb){
                        var _icon = (valb[0]=='fon-tabel-dt') ? 'pencil' : valb[0];
                        var _cssa = (valb[1]=='') ? '' : ' text-'+valb[1];
                        var _cssb = (valb[1]=='') ? ' normal' : ' btn-'+valb[1];
                        if (ini.config.txtbutton === true) {
                            _tbtn1 += '<button class="btn tbl-rowbtn d-none d-lg-inline-block '+valb[0]+_cssb+' me-2" type="button">'+aTombol[valb[0]]+'</button>';
                            _tbtn2 += '<a class="table-icon d-lg-none '+valb[0]+_cssa+'" tabindex="-1" title="'+aTombol[valb[0]]+'"><i class="icon cui-'+_icon+'"></i></a>';
                        } else {
                            _tbtn2 += '<a class="table-icon '+valb[0]+_cssa+'" title="'+aTombol[valb[0]]+'"><i class="icon cui-'+_icon+'"></i></a>';
                        }
                    })
                }
                if (val[0].checkbox) {
                    _tbtn1 += '<div><input class="form-check-input" type="checkbox" id="'+ChkField+'_cbox_'+val[0].checkbox.field+'" value="'+val[0].checkbox.value+'"></div>';
                }
                strBody += '<td class="">'+_tbtn1+_tbtn2+'</td>';
            }
            strBody += '</tr>';
        });
        strBody += '</tbody>';

        $(divData).append(strTab + strHead + strBody + strFoot + '</table>');
        var opsi = { responsive: true, columnDefs: [{responsivePriority:1,targets:0},{responsivePriority:2,targets:1}], lengthMenu: [], dom: '<"row"<"col-sm-12"tr>><"row"<"col-sm-12 col-lg-auto"<"toolbar">><"col-sm-3 col-lg-auto ms-lg-auto"l><"col-sm-9 col-lg-auto"p>>', autoWidth: false, processing: true, searching: false, columns: aSortCol, order: [[iCol, (ini.config.sort_dir=='A'?'asc':'desc')]], pageLength: ini.config.row_count, stateSave: true, stateSaveCallback: function(sett,data){localStorage.setItem(ini.id,JSON.stringify(data));}, stateLoadCallback: function(sett){try{var data=JSON.parse(localStorage.getItem(ini.id))??{};data.length=ini.config.row_count;return data;}catch(e){console.log(e)}} };
        /** start live db **/
        if (ini.config.live){
            var post_data = {};
            $(vars.data).map(function(key, item){
                var keyval = item.split("=");
                if (keyval[0] == 'SID') keyval[1] = parseInt(Math.random() * 99999999);
                if ((''+keyval[1]).indexOf('%') != -1) keyval[1] = decodeURIComponent(keyval[1]);
                return post_data[keyval[0]] = keyval[1];
            });
            opsi.serverSide = true;
            opsi.ajax = {
                url: vars.url,
                type: 'POST',
                data: post_data,
                dataFilter: function(data){
                    var json = {}, jsono = {'draw':0,'recordsTotal':0,'recordsFiltered':0,'data':[]};
                    try {
                        json = jQuery.parseJSON(data);
                        if (!jQuery.isEmptyObject(json) && json[0].draw){
                            jsono.draw = json[0].draw;
                            jsono.recordsTotal = json[0].recordsTotal;
                            jsono.recordsFiltered = json[0].recordsFiltered;
                            jsono.data = json[0].data;
                        }
                    } catch (e){
                        console.log(e);
                    }

                    $.each(jsono.data, function(key, val){
                        var optval = val[0]; jsono.data[key].shift();
                        if (ini.option.length > 0) {
                            var _tbtn1 = "", _tbtn2 = "";
                            if (optval.rowcss) {
                                _tbtn1 += '<span class="rowcss '+optval.rowcss+'"></span>';
                            }
                            if (optval.button.length > 0) {
                                $.each(optval.button, function(keyb, valb){
                                    var _icon = (valb[0]=='fon-tabel-dt') ? 'pencil' : valb[0];
                                    var _cssa = (valb[1]=='') ? '' : ' text-'+valb[1];
                                    var _cssb = (valb[1]=='') ? ' normal' : ' btn-'+valb[1];
                                    if (ini.config.txtbutton === true) {
                                        _tbtn1 += '<button class="btn tbl-rowbtn d-none d-lg-inline-block '+valb[0]+_cssb+' me-2" type="button">'+aTombol[valb[0]]+'</button>';
                                        _tbtn2 += '<a class="table-icon d-lg-none '+valb[0]+_cssa+'" tabindex="-1" title="'+aTombol[valb[0]]+'"><i class="icon cui-'+_icon+'"></i></a>';
                                    } else {
                                        _tbtn2 += '<a class="table-icon '+valb[0]+_cssa+'" title="'+aTombol[valb[0]]+'"><i class="icon cui-'+_icon+'"></i></a>';
                                    }
                                })
                            }
                            if (optval.checkbox) {
                                _tbtn1 += '<div><input class="form-check-input" type="checkbox" id="'+ChkField+'_cbox_'+optval.checkbox.field+'" value="'+optval.checkbox.value+'"></div>';
                            }
                            jsono.data[key].push(_tbtn1+_tbtn2);
                        }
                    });
                    return JSON.stringify(jsono);
                }
            };
        }
        /** end live db **/
        if (ini.option.length > 0) opsi.columnDefs.push({responsivePriority:3,targets:-1}, {orderable:false,targets:-1});
        if (ini.navi.func_navi == '') opsi.paging = false;

        if (ini.config.scrollbar == true) {
            opsi.scrollY = ini.config.scroll_height;
            opsi.deferRender = true;
            opsi.scroller = { loadingIndicator: true };
        }

        var _navibar = '';
        if (ini.navi.button) {
            var _dis = '', _bcss = 'btn-info';
            if (ini.config.nobutton === true){
                _dis = ' disabled=true';
                _bcss = 'btn-secondary';
            }
            _navibar += '<div class="row"><div class="col">';
            $.each(ini.navi.button, function(key, val){
                _navibar += '<button type="button" class="btn rounded-pill '+_bcss+' me-2"'+_dis+' onclick="'+val.func+'();">'+val.label+'</button>';
            });
            _navibar += '</div></div>';
        }
        opsi.initComplete = function(settings,json){
            if (ini.navi.button) $('#'+ini.id+'Tabel_wrapper div.toolbar').html(_navibar);
        }
        opsi.drawCallback = function(settings){
            // apply rowcss to tr/row
            $('#'+ini.id+'Tabel tbody td:last-child:has(span.rowcss)').each(function(){
                var span = $(this).find('span').attr('class').replace('rowcss ','');
                $(this).parent().addClass(span);
            });
            // create hover title for awr class td
            $('#'+ini.id+'Tabel tbody td[class*="awr"]').each(function(){
                $(this).attr('title', $(this).html().replace(/<br\s*[\/]?>/gi,'\n').replace(/(<([^>]+)>)/gi,''));
            });
            // generate pagelength option
            var api = this.api(), menudiv = $('select[name="'+ini.id+'Tabel_length"]');
            var record = api.page.info().recordsTotal ?? 0, _lmenu = [];
            $.each([10,25,50,100], function(key, val){
                if (val > record && 10 < val) return false;
                _lmenu.push(val);
            });
            if (10 < record && $.inArray(record, [25,50,100]) == -1){
                _lmenu.push(record);
            }
            var _select = menudiv.val() ?? 10;
            menudiv.empty();
            $.each(_lmenu, function(key, val){
                $('<option/>', {'value':val, 'text':val}).appendTo(menudiv);
            });
            menudiv.val(_select);
            // apply select function
            if (ini.config.func_select){
                $('#'+ini.id+'Tabel tbody').on('click', 'tr', function(){
                    var dta = otabel.row(this).data();
                    var fnc = ini.config.func_select.split('.');
                    if (typeof window[fnc[0]][fnc[1]] === 'function') { window[fnc[0]][fnc[1]](dta); }
                    else console.warn('fungsi '+ini.config.func_select+' tidak ada !');
                });
            }
            // apply option of record / row
            if (ini.option.length > 0){
                $('#'+ini.id+'Tabel thead input[type=checkbox][id*=_cbhead_]').off('change');
                $('#'+ini.id+'Tabel tbody input[type=checkbox][id*=_cbox_]').off('change');
                $('#'+ini.id+'Tabel tbody').off('click');
                $.each(ini.option, function(key, val){
                    if (val.type == 'checkbox'){
                        $('#'+ini.id+'Tabel thead input[type=checkbox][id*=_cbhead_]').on('change', function(){
                            var _chek = this.checked;
                            this.value = (this.checked ? val.avalue[0] : val.avalue[1]);
                            $('#'+ini.id+'Tabel tbody input[type=checkbox][id*='+val.field+'_cbox_]').each(function(){
                                this.checked = _chek;
                                this.value = (this.checked ? val.avalue[0] : val.avalue[1]);
                            });
                        });
                        $('#'+ini.id+'Tabel tbody input[type=checkbox][id*=_cbox_]').on('change', function(){
                            this.value = (this.value == val.avalue[1] ? val.avalue[0] : val.avalue[1]);
                            this.checked = this.value == val.avalue[0];
                        });
                    } else if (val.type == 'button'){
                        if (val.icon == 'fon-tabel-dt'){
                            $('#'+ini.id+'Tabel tbody').on('click', '.fon-tabel-dt', function(){
                                var tr = $(this).closest('tr');
                                var row = otabel.row(tr);
                                if (row.child.isShown()){
                                    row.child.hide();
                                    tr.removeClass('shown');
                                }else{
                                    var fnc = val.func.split('.');
                                    if (typeof window[fnc[0]][fnc[1]] === 'function'){
                                        var _dt_id = $('<div id="TD_Fonia_' + parseInt(Math.random() * 99999999) + '" />');
                                        row.child(_dt_id).show();
                                        tr.addClass('shown');
                                        window[fnc[0]][fnc[1]](row.data(), _dt_id);
                                    } else console.warn('fungsi '+val.func+' tidak ada !');
                                }
                            });
                        } else {
                            $('#'+ini.id+'Tabel tbody').on('click', '.'+val.icon, function(){
                                var dta = otabel.row($(this.closest('tr'))).data();
                                var fnc = val.func.split('.');
                                if (typeof window[fnc[0]][fnc[1]] === 'function') { window[fnc[0]][fnc[1]](dta, this); }
                                else if (typeof window[fnc[0]] === 'function') { window[fnc[0]](dta, this); }
                                else console.warn('fungsi '+val.func+' tidak ada !');
                            });
                        }
                    }
                });
            }
            //if (ini.config.live){
                $('#'+ini.id+'Tabel tbody').on('click', 'td.dtr-control', function(){
                    var tr = $(this).closest('tr');
                    var row = otabel.row(tr);
                    if (row.child.isShown()){
                        row.child.hide();
                        tr.removeClass('dt-hasChild parent');
                    }else{
                        var ind = 0;
                        var tampil = '<ul class="list-unstyled">';
                        api.columns().every(function() {
                            var kol = this.header();
                            if (kol.className.indexOf('dtr-hidden') != -1){
                                tampil += '<li class="py-1"><span class="d-inline-block" style="min-width:100px;">' + kol.textContent + '</span><span class="text-break">' + row.data()[ind] + '</span></li>';
                            }
                            ind++;
                        })
                        tampil += '</ul>';
                        row.child(tampil).show();
                        tr.addClass('dt-hasChild parent');
                    }
                });
            //}
        }
        var otabel = $('#'+ini.id+'Tabel').DataTable(opsi);
    };
    this.construct(options);
};

if (jQuery.validator) {
    jQuery.validator.setDefaults({
        ignore: ':hidden, [readonly=readonly]',
        submitHandler: function(){
            return;
        },
        invalidHandler: function(){
            FoniaJS.showError('Mohon lengkapi semua field yang berwarna merah !')
        },
        highlight: function highlight(element) {
            $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function unhighlight(element) {
            $(element).addClass('is-valid').removeClass('is-invalid');
        },
        errorPlacement: function(error, element){
            return;
        }
    });

    jQuery.validator.addMethod('double_id', function(value, el, param) {
        return this.optional(el) || FoniaJS.StrToFloat(value) >= param;
    }, 'Nilai lebih dari Nol');
    jQuery.validator.addMethod('filesize', function(value, el, param) {
         return this.optional(el) || (el.files[0].size <= param);
    }, 'Ukuran file tidak diizinkan');
    jQuery.validator.addMethod('notEqual', function(value, el, param) {
        return this.optional(el) || value != param;
    }, 'Ganti pilihan yang lain');
}

if (jQuery.fn.dataTable) {
    jQuery.fn.dataTable.ext.order['val-emptylast'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var _val = $(td).html();
            return (_val=='') ? Number.MAX_SAFE_INTEGER : 0;
        });
    }
    jQuery.fn.dataTable.ext.order['val-zerolast'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var _val = $(td).html();
            return (_val==0) ? Number.MAX_SAFE_INTEGER : _val;
        });
    }
    jQuery.fn.dataTable.ext.order['val-fnumber'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var val = $(td).text().trim();
            return val.replace( /[\,]/g, "" );
        });
    }
    jQuery.fn.dataTable.ext.order['val-jam'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var val = $(td).text().trim();
            return moment(val, "HH:mm:ss").format("X");
        });
    }
    jQuery.fn.dataTable.ext.order['val-tgl'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var val = $(td).text().trim();
            return moment(val, "DD-MM-YYYY").format("X");
        });
    }
    jQuery.fn.dataTable.ext.order['val-tgljam'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var val = $(td).text().trim();
            return moment(val, "DD-MM-YYYY HH:mm:ss").format("X");
        });
    }
    jQuery.fn.dataTable.ext.order['val-tgljams'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var val = $(td).text().trim();
            return moment(val, "DD.MM.YY HH:mm:ss").format("X");
        });
    }
    jQuery.fn.dataTable.ext.order['dom-checkbox'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            return $('input', td).prop('checked') ? '1' : '0';
        });
    }
    jQuery.fn.dataTable.ext.order['dom-select-zerolast'] = function( settings, col ){
        return this.api().column( col, {order:'index'} ).nodes().map( function( td, i ){
            var _val = $('select', td).val();
            return (_val==0) ? Number.MAX_SAFE_INTEGER : _val;
        });
    }
    jQuery.extend(jQuery.fn.dataTable.ext.classes, {
        sLengthSelect: "form-select text-center",
    });
    jQuery.extend(jQuery.fn.dataTable.defaults.oLanguage, {
        "oAria": {
            "sSortAscending": ": aktifkan urutan kolom keatas",
            "sSortDescending": ": aktifkan urutan kolom kebawah"
        },
        "oPaginate": {
            "sFirst": "<i class=\"cui-media-step-backward\"></i>",
            "sPrevious": "<i class=\"cui-media-skip-backward\"></i>",
            "sNext": "<i class=\"cui-media-skip-forward\"></i>",
            "sLast": "<i class=\"cui-media-step-forward\"></i>"
        },
        "sEmptyTable": "Tidak ada data yang tersedia pada tabel ini",
        "sProcessing": "",
        "sLengthMenu": "_MENU_",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "sLoadingRecords": "Memuat...",
        "sSearchPlaceholder": "Cari Kata Kunci",
        "sDecimal": ",",
        "sThousands": "."
    });
}
