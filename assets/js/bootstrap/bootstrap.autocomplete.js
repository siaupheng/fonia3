var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BaseResolver = (function () {
    function BaseResolver(options) {
        this._settings = $.extend(true, {}, this.getDefaults(), options);
    }
    BaseResolver.prototype.getDefaults = function () {
        return {};
    };
    BaseResolver.prototype.getResults = function (limit, start, end) {
        return this.results;
    };
    BaseResolver.prototype.search = function (q, cbk) {
        cbk(this.getResults());
    };
    return BaseResolver;
}());

var AjaxResolver = (function (_super) {
    __extends(AjaxResolver, _super);
    function AjaxResolver(options) {
        return _super.call(this, options) || this;
    }
    AjaxResolver.prototype.getDefaults = function () {
        return {
            url: '',
            method: 'post',
            queryKey: 'Auto_Query',
            extraData: {},
            timeout: 0,
            requestThrottling: 500
        };
    };
    AjaxResolver.prototype.search = function (q, cbk) {
        var _this = this;
        if (this.jqXHR != null) {
            this.jqXHR.abort();
        }
        var data = {};
        data[this._settings.queryKey] = q;
        $.extend(data, this._settings.extraData);
        if (this.requestTID) {
            window.clearTimeout(this.requestTID);
        }
        this.requestTID = window.setTimeout(function () {
            _this.jqXHR = $.ajax(_this._settings.url, {
                method: _this._settings.method,
                data: data,
                timeout: _this._settings.timeout
            });
            _this.jqXHR.done(function (result) {
                //gnips: result in object
                cbk((undefined !== result.autodata) ? result.autodata : result);
            });
            _this.jqXHR.fail(function (err) {
                var _a;
                (_a = _this._settings) === null || _a === void 0 ? void 0 : _a.fail(err);
            });
            _this.jqXHR.always(function () {
                _this.jqXHR = null;
            });
        }, this._settings.requestThrottling);
    };
    return AjaxResolver;
}(BaseResolver));

var DropdownV5 = (function () {
    function DropdownV5(e, formatItemCbk, autoSelect, noResultsText) {
        this.initialized = false;
        this.shown = false;
        this.items = [];
        this.ddMouseover = false;
        this._$el = e;
        this.formatItem = formatItemCbk;
        this.autoSelect = autoSelect;
        this.noResultsText = noResultsText;
    }
    DropdownV5.prototype.getElPos = function () {
        var pos = $.extend({}, this._$el.position(), {
            height: this._$el[0].offsetHeight
        });
        return pos;
    };
    DropdownV5.prototype.init = function () {
        var _this = this;
        var pos = this.getElPos();
        this._dd = $('<div />');
        this._dd.addClass('bootstrap-autocomplete dropdown-menu');
        this._dd.insertAfter(this._$el);
        this._dd.css({ top: pos.top + this._$el.outerHeight(), left: pos.left, width: this._$el.outerWidth() });
        this._dd.on('click', '.dropdown-item', function (evt) {
            //console.log('clicked', evt.currentTarget);
            //console.log($(evt.currentTarget));
            var item = $(evt.currentTarget).data('item');
            _this.itemSelectedLaunchEvent(item);
            evt.preventDefault();
        });
        this._dd.on('keyup', function (evt) {
            if (_this.shown) {
                switch (evt.which) {
                    case 27:
                        _this.hide();
                        _this._$el.focus();
                        break;
                }
                return false;
            }
        });
        this._dd.on('mouseenter', function (evt) {
            _this.ddMouseover = true;
        });
        this._dd.on('mouseleave', function (evt) {
            _this.ddMouseover = false;
        });
        this._dd.on('mouseenter', '.dropdown-item', function (evt) {
            if (_this.haveResults) {
                $(evt.currentTarget).closest('div').find('.dropdown-item.active').removeClass('active');
                $(evt.currentTarget).addClass('active');
                _this.mouseover = true;
            }
        });
        this._dd.on('mouseleave', '.dropdown-item', function (evt) {
            _this.mouseover = false;
        });
        this.initialized = true;
    };
    DropdownV5.prototype.checkInitialized = function () {
        if (!this.initialized) {
            this.init();
        }
    };
    Object.defineProperty(DropdownV5.prototype, "isMouseOver", {
        get: function () {
            return this.mouseover;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DropdownV5.prototype, "isDdMouseOver", {
        get: function () {
            return this.ddMouseover;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DropdownV5.prototype, "haveResults", {
        get: function () {
            return (this.items.length > 0);
        },
        enumerable: false,
        configurable: true
    });
    DropdownV5.prototype.focusNextItem = function (reversed) {
        if (this.haveResults) {
            var currElem = this._dd.find('.dropdown-item.active');
            var nextElem = reversed ? currElem.prev() : currElem.next();
            if (nextElem.length === 0) {
                nextElem = reversed ? this._dd.find('.dropdown-item').last() : this._dd.find('.dropdown-item').first();
            }
            currElem.removeClass('active');
            nextElem.addClass('active');
            this.scrollTo(nextElem);
        }
    };
    DropdownV5.prototype.focusPreviousItem = function () {
        this.focusNextItem(true);
    };
    DropdownV5.prototype.selectFocusItem = function () {
        this._dd.find('.dropdown-item.active').trigger('click');
    };
    Object.defineProperty(DropdownV5.prototype, "isItemFocused", {
        get: function () {
            if (this._dd && this.isShown() && (this._dd.find('.dropdown-item.active').length > 0)) {
                return true;
            }
            return false;
        },
        enumerable: false,
        configurable: true
    });
    DropdownV5.prototype.show = function () {
        if (!this.shown) {
            this._dd.addClass('show');
            this.shown = true;
            this._$el.trigger('autocomplete.dd.shown');
        }
    };
    DropdownV5.prototype.isShown = function () {
        return this.shown;
    };
    DropdownV5.prototype.hide = function () {
        if (this.shown) {
            this._dd.removeClass('show');
            this.shown = false;
            this._$el.trigger('autocomplete.dd.hidden');
        }
    };
    DropdownV5.prototype.updateItems = function (items, searchText) {
        this.items = items;
        this.searchText = searchText;
        this.refreshItemList();
    };
    DropdownV5.prototype.showMatchedText = function (text, qry) {
        var startIndex = text.toLowerCase().indexOf(qry.toLowerCase());
        if (startIndex > -1) {
            var endIndex = startIndex + qry.length;
            return text.slice(0, startIndex) + '<b>'
                + text.slice(startIndex, endIndex) + '</b>'
                + text.slice(endIndex);
        }
        return text;
    };
    DropdownV5.prototype.refreshItemList = function () {
        var _this = this;
        this.checkInitialized();
        this._dd.empty();
        var liList = [];
        if (this.items.length > 0) {
            this.items.forEach(function (item) {
                var itemFormatted = _this.formatItem(item);
                if (typeof itemFormatted === 'string') {
                    itemFormatted = { text: itemFormatted };
                }
                var itemText;
                var itemHtml;
                itemText = _this.showMatchedText(itemFormatted.text, _this.searchText);
                if (itemFormatted.html !== undefined) {
                    itemHtml = itemFormatted.html;
                }
                else {
                    itemHtml = itemText;
                }
                var disabledItem = itemFormatted.disabled;
                var li = $('<a >');
                li.addClass('dropdown-item')
                    //css bisa dimasukkan ke style default + cursor select
                    //.css({ 'overflow': 'hidden', 'text-overflow': 'ellipsis' })
                    .html(itemHtml)
                    .data('item', item);
                if (disabledItem) {
                    li.addClass('disabled');
                }
                liList.push(li);
            });
            this._dd.append(liList);
            this.show();
        }
        else {
            if (this.noResultsText === '') {
                this.hide();
            }
            else {
                var li = $('<a >');
                li.addClass('dropdown-item disabled')
                    .html(this.noResultsText);
                liList.push(li);
                this._dd.append(liList);
                this.show();
            }
        }
    };
    DropdownV5.prototype.itemSelectedLaunchEvent = function (item) {
        this._$el.trigger('autocomplete.select', item);
    };
    DropdownV5.prototype.scrollTo = function (el) {
        var _he = this._dd.height(), _to = this._dd.scrollTop(), _ito = el.position().top, _ihe = el.outerHeight(),
            _nto = _to, _wra = false;
        if (_ito < 0){
            _wra = el.is(':first-child');
            _nto = _wra ? 0 : _to - _he / 2;
        } else if (_ito > _he - _ihe) {
            _wra = el.is(':last-child') && _ito > _he;
            _nto = _wra ? (_ito + _ihe - _he) : _to + _he / 2;
        }
        this._dd.scrollTop(_nto);
    };
    return DropdownV5;
}());

var AutoComplete = (function () {
    function AutoComplete(element, options) {
        this._selectedItem = null;
        this._defaultValue = null;
        this._defaultText = null;
        this._settings = {
            resolver: 'ajax',
            resolverSettings: {},
            minLength: 3,
            valueKey: 'value',
            formatResult: this.defaultFormatResult,
            autoSelect: true,
            noResultsText: 'Pencarian Tidak Ketemu',
            preventEnter: false,
            events: {
                typed: null,
                searchPre: null,
                search: null,
                searchPost: null,
                select: null,
                focus: null,
            },
            //gnips: adding callback and keydown function
            selectFunc: function(){},
            keydownFunc: function(){}
        };
        this._el = element;
        this._$el = $(this._el);
        this.manageInlineDataAttributes();
        if (typeof options === 'object') {
            this._settings = $.extend(true, {}, this.getSettings(), options);
        }
        this.init();
    }
    AutoComplete.prototype.manageInlineDataAttributes = function () {
        var s = this.getSettings();
        if (this._$el.data('url')) {
            s.resolverSettings.url = this._$el.data('url');
        }
        if (this._$el.data('default-value')) {
            this._defaultValue = this._$el.data('default-value');
        }
        if (this._$el.data('default-text')) {
            this._defaultText = this._$el.data('default-text');
        }
        if (this._$el.data('noresults-text') !== undefined) {
            s.noResultsText = this._$el.data('noresults-text');
        }
    };
    AutoComplete.prototype.getSettings = function () {
        return this._settings;
    };
    AutoComplete.prototype.init = function () {
        this.bindDefaultEventListeners();
        if (this._settings.resolver === 'ajax') {
            this.resolver = new AjaxResolver(this._settings.resolverSettings);
        }
        this._dd = new DropdownV5(this._$el, this._settings.formatResult, this._settings.autoSelect, this._settings.noResultsText);
    };
    AutoComplete.prototype.bindDefaultEventListeners = function () {
        var _this = this;
        this._$el.on('keydown', function (evt) {
            //gnips: call keydown function
            ('' == _this._$el.val() && _this._settings.keydownFunc(evt.which));
            switch (evt.which) {
                case 9:
                    if (_this._dd.isItemFocused) {
                        _this._dd.selectFocusItem();
                    }
                    _this._dd.hide();
                    break;
                case 13:
                    if (_this._dd.isItemFocused) {
                        _this._dd.selectFocusItem();
                    }
                    _this._dd.hide();
                    if (_this._settings.preventEnter) {
                        evt.preventDefault();
                    }
                    break;
                case 40:
                    _this._dd.focusNextItem();
                    break;
                case 38:
                    _this._dd.focusPreviousItem();
                    break;
            }
        });
        this._$el.on('keyup', function (evt) {
            switch (evt.which) {
                case 16:
                case 17:
                case 18:
                case 39:
                case 37:
                case 36:
                case 35:
                    break;
                case 13:
                    _this._dd.hide();
                    break;
                case 27:
                    _this._dd.hide();
                    break;
                case 40:
                    break;
                case 38:
                    break;
                default:
                    _this._selectedItem = null;
                    var newValue = _this._$el.val();
                    _this.handlerTyped(newValue);
            }
        });
        this._$el.on('blur', function (evt) {
            if (!_this._dd.isMouseOver && _this._dd.isDdMouseOver && _this._dd.isShown()) {
                setTimeout(function () { _this._$el.focus(); });
                _this._$el.focus();
            }
            else if (!_this._dd.isMouseOver) {
                _this._dd.hide();
            }
        });
        this._$el.on('autocomplete.select', function (evt, item) {
            _this._selectedItem = item;
            _this.itemSelectedDefaultHandler(item);
        });
        this._$el.on('paste', function (evt) {
            setTimeout(function () {
                _this._$el.trigger('keyup', evt);
            }, 0);
        });
    };
    AutoComplete.prototype.handlerTyped = function (newValue) {
        if (this._settings.events.typed !== null) {
            newValue = this._settings.events.typed(newValue, this._$el);
            if (!newValue) {
                return;
            }
        }
        if (newValue.length >= this._settings.minLength) {
            this._searchText = newValue;
            this.handlerPreSearch();
        }
        else {
            this._dd.hide();
        }
    };
    AutoComplete.prototype.handlerPreSearch = function () {
        if (this._settings.events.searchPre !== null) {
            var newValue = this._settings.events.searchPre(this._searchText, this._$el);
            if (!newValue)
                return;
            this._searchText = newValue;
        }
        this.handlerDoSearch();
    };
    AutoComplete.prototype.handlerDoSearch = function () {
        var _this = this;
        if (this._settings.events.search !== null) {
            this._settings.events.search(this._searchText, function (results) {
                _this.postSearchCallback(results);
            }, this._$el);
        }
        else {
            if (this.resolver) {
                this.resolver.search(this._searchText, function (results) {
                    _this.postSearchCallback(results);
                });
            }
        }
    };
    AutoComplete.prototype.postSearchCallback = function (results) {
        if (this._settings.events.searchPost) {
            results = this._settings.events.searchPost(results, this._$el);
            if ((typeof results === 'boolean') && !results)
                return;
        }
        this.handlerStartShow(results);
    };
    AutoComplete.prototype.handlerStartShow = function (results) {
        //gnips: adding auto select when barcode result of 1 item only
        if (1 == results.length && 1 == (void 0 !== results[0].bc ? results[0].bc : 0)) {
            this._$el.trigger('autocomplete.select', results[0]),
            this._dd.hide();
        } else this._dd.updateItems(results, this._searchText);
    };
    AutoComplete.prototype.itemSelectedDefaultHandler = function (item) {
        if (item != null) {
            var itemFormatted = this._settings.formatResult(item);
            if (typeof itemFormatted === 'string') {
                itemFormatted = { text: itemFormatted };
            }
            this._$el.val(itemFormatted.text);
        }
        else {
            this._$el.val('');
        }
        this._selectedItem = item;
        //gnips: callback with selected item
        this._settings.selectFunc(item);
        this._dd.hide();
    };
    AutoComplete.prototype.defaultFormatResult = function (item) {
        if (typeof item === 'string') {
            return { text: item };
        }
        else if (item.text) {
            return item;
        }
        else {
            return { text: item.toString() };
        }
    };
    AutoComplete.prototype.manageAPI = function (APICmd, params) {
        if (APICmd === 'set') {
            this.itemSelectedDefaultHandler(params);
        }
        else if (APICmd === 'clear') {
            this.itemSelectedDefaultHandler(null);
        }
        else if (APICmd === 'show') {
            this._$el.trigger('keyup');
        }
        else if (APICmd === 'updateResolver') {
            this.resolver = new AjaxResolver(params);
        }
    };
    AutoComplete.NAME = 'autoComplete';
    return AutoComplete;
}());

(function ($, window, document) {
    $.fn[AutoComplete.NAME] = function (optionsOrAPI, optionalParams) {
        return this.each(function () {
            var pluginClass;
            pluginClass = $(this).data(AutoComplete.NAME);
            if (!pluginClass) {
                pluginClass = new AutoComplete(this, optionsOrAPI);
                $(this).data(AutoComplete.NAME, pluginClass);
            }
            pluginClass.manageAPI(optionsOrAPI, optionalParams);
        });
    };
})(jQuery, window, document);
