/*******************************************************************

    Module        : /js3/qz/qz-start.js
    Desc.         : JS Setup QZ Connection
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : May 27th, 2007.
    Last Modified : January 9th, 2021.

    (c) 2008 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

function startConnection() {
    if (!qz.websocket.isActive()) {
        _updateState('Waiting');
        qz.websocket.connect().then(function() {
            _updateState('Active');
            _findDefaultPrinter();
        });
    } else {
        console.info('An active connection with QZ already exists.');
    }
}

function endConnection(){
    if (qz.websocket.isActive()) {
        qz.websocket.disconnect().then(function() {
            _updateState('Inactive');
        });
    } else {
        console.info('No active connection with QZ exists.');
    }
}

function _findDefaultPrinter(){
    qz.printers.getDefault().then(function(data) {
        var _printer = data;
        if (typeof data === 'object' && data.name == undefined) {
            if (data.file != undefined) _printer = "FILE: " + data.file;
            if (data.host != undefined) _printer = "HOST: " + data.host + ":" + data.port;
        } else {
            if (data.name != undefined) _printer = data.name;
            if (data== undefined) _printer = 'NONE';
        }
        FoniaJS._def_printer = data;
        FoniaJS._def_printname = _printer;
        $(_divStatus).html($(_divStatus).html() + ' // ' + _printer);
    });
}

function _updateState(text){
    $(_divStatus).html(text);
    if (text == "Active") {
        $(_divStatus).css("background-color", "#AAFFAA");
    } else {
        $(_divStatus).css("background-color", "#FFFFC0");
    }
}

var _divStatus = '#divQZStatus';

if (typeof qz !== 'undefined') {
    qz.security.setCertificatePromise(function(resolve, reject) {
        jQuery.ajax({ url:FoniaJS.WEB_PATH+'assets/signing/cert.pem', cache:false, dataType:'text' }).then(resolve, reject);
    });
    qz.security.setSignaturePromise(function(toSign) {
        return function(resolve, reject) {
            jQuery.post(FoniaJS.WEB_PATH+"assets/signing/sign.php?req="+toSign).then(resolve, reject);
        };
    });
    qz.websocket.setClosedCallbacks(function(evt) {
        _updateState('Inactive');
        if (evt.reason) console.info("Connection closed: " + evt.reason);
    });
    qz.websocket.setErrorCallbacks(function(evt){
        _updateState('Error');
        if (evt.target != undefined) {
            var err = (evt.target.readyState >= 2) ? "Connection to QZ Tray was closed" : "A connection error occurred, check log for details";
            console.info(err)
        }
    });
    startConnection();
}
