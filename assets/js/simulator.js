/*******************************************************************

    Module        : /assets/js/simulator.js
    Desc.         : JS Class Simulator Data dalam Code
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : March 18th, 2021.
    Last Modified : April 5th, 2022.

    (c) 2021 - 2022, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

var cSIMULATOR = function(url, options){
    var vars = {
        'url': '',
        'param': []
    };
    this.construct = function(url, options){
        $.extend(vars, options);
        vars.url = url;
    };
    this.set_url = function (url){
        vars.url = url;
    };
    this.add_param = function (par_id, par_val){
        var _getvalue = (par_val) ? par_id + "=" + escape(par_val) : par_id;
        vars.param.push(_getvalue);
    },
    this.doPreview = function(){
        if (!vars.url) console.warn(vars.param + ", cek url php!");
        vars.data = FoniaJS.RequestParam(vars.param);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: function(data){
                if (data.length == 0) {
                    FoniaJS.showInfo('Tidak ada data !');
                    return false;
                }
                var _divd = '';
                for (var no = 0; no < data.length; no++) {
                    _divd += '<div class="sim-code px-2 py-2 mb-1 border border-info">' + data[no] + '</div>';
                }
                var _wtxt = 
                    '<div class="modal fade" id="winAPPSim" tabindex="-1" role="dialog" data-bs-keyboard="false" data-bs-show="false" data-bs-backdrop="static" aria-labelledby="winAPPSimLabel" aria-hidden="true">'+
                    '<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-fs" role="document">'+
                    '<div class="modal-content">'+
                    '<div class="modal-header"><h5 class="modal-title" id="winAPPSimLabel">Code Simulator</h5><div class="ml-auto"><button class="btn btn-cetak" id="tombSalin" type="button" aria-label="Salin Semua"><i class="icon cui-copy"></i><span aria-hidden="true">&nbsp;Salin Semua</span></button><button class="btn btn-cetak" type="button" data-bs-dismiss="modal" aria-label="Tutup"><i class="icon cui-x"></i><span aria-hidden="true">&nbsp;Tutup</span></button></div></div>'+
                    '<div class="modal-body">'+_divd+'</div>'+
                    '</div></div></div>';
                if ($('#winAPPSim')) $('#winAPPSim').modal('dispose').remove();
                $(_wtxt).on('shown.bs.modal', function(e){
                    $('#winAPPSim').find('#tombSalin').click(function(){
                        window.getSelection().removeAllRanges();
                        $('.sim-code').each(function(i, ele) {
                            var range = document.createRange();
                            range.selectNode(ele);
                            window.getSelection().addRange(range);
                        });
                        document.execCommand('copy');
                        window.getSelection().removeAllRanges();
                        FoniaJS.showInfo('Sukses salin semua Kode !');
                    });
                    $('#winAPPSim').find('.sim-code').click(function(){
                        var range = document.createRange();
                        range.selectNode(this);
                        window.getSelection().removeAllRanges();
                        window.getSelection().addRange(range);
                        document.execCommand('copy');
                        window.getSelection().removeAllRanges();
                        //$(this).addClass('bg-success');
                        $(this).addClass('d-none');
                    });
                }).on('hidden.bs.modal', function(e){
                    $(this).remove();
                }).modal('show');
            },
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
            dataType: 'json'
        });
    };
    this.construct(url, options);
};
