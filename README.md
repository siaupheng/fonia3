# fonia3

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Total Downloads][ico-downloads]][link-downloads]

fonia3 adalah framework webbase aplikasi yang dibuat dengan basis kode PHP dan digabungkan dengan beberapa repo lain, sehingga menghasilkan webbase framework yang efisien dan efektif.

Maintenance versi repo tambahan, akan dilakukan secara periodik dengan mempertimbangkan kesiapan infrastruktur framework secara utuh.


## Install

Via Composer

``` bash
$ composer require siaupheng/fonia3
```

## Security

Jika anda menemukan masalah keamanan, silahkan email siaupheng@3fonia.com dengan permasalahan tersebut.

## Credits

- [Siau Pheng][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/siaupheng/fonia3.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/siaupheng/fonia3/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/siaupheng/fonia3.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/siaupheng/fonia3.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/siaupheng/fonia3.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/siaupheng/fonia3
[link-downloads]: https://packagist.org/packages/siaupheng/fonia3
[link-author]: https://github.com/siaupheng
[link-contributors]: ../../contributors
